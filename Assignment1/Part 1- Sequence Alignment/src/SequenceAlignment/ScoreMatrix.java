package SequenceAlignment;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ScoreMatrix {
	final int A = 0;
	final int T = 1;
	final int G = 2;
	final int C = 3;
	final int U = 4;
	final int N = 5;
	final int SPACE = 6;

	private int[][] matrix;
	private int a;
	private int b;

	public ScoreMatrix(){
		this.matrix = new int [7][7];
		this.a = 0;
		this.b = 0;
	}

	/**
	 * This function parses the parameters of the affine gap function from the given score matrix file
	 * @param line the line to parse
	 * @return parsed int value 
	 */
	private int parseParam(String line){
		int i = line.length() - 1;
		while (line.charAt(i) != ' '){
			i--;
		}
		return Integer.parseInt(line.substring(++i));
	}
	
	/**
	 * This function parses the rows of the score matrix
	 * @param line the line to parse
	 * @return parsed row values
	 */
	private int[] parseRow(String line){
		int rowVals[] = new int[7];
		int j = 0;
		int i = 1;
		while (i<line.length()){
			if (line.charAt(i) == ' '){
				i++;
				continue;
			}
			int k = i;
			if (line.charAt(i) == '-') {
				while ( (k<line.length()) && (line.charAt(k) != ' ') ){
					k++;
				}
				rowVals[j] = Integer.parseInt(line.substring(i, k));
			} else {
				while ( (k<line.length()) && (line.charAt(k) != ' ') ){
					k++;
				}
				rowVals[j] = Integer.parseInt(line.substring(i, k));
			}
			i = k;
			j++;
		}
		return rowVals;
	}

	/**
	 * This function parses the given score matrix file
	 * @param fileName the given file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void ParseFile (String fileName) throws FileNotFoundException, IOException{
		int[][] matrix = new int[7][7];
		BufferedReader br =null;
		try{ // Check if full path of score matrix file is given
			br = new BufferedReader(new FileReader(fileName));
		}
		catch(FileNotFoundException e){ 
			// Only name of score matrix file is given.
			// extrapolate the current working directory. 
			br = new BufferedReader(new FileReader(System.getProperty("user.dir")+fileName));
		}
		
		String line;
		
		while ((line = br.readLine()) != null) {
			switch (line.charAt(0)){
				case '#': 
					break;
				case 'A':
					if (line.charAt(2) == ' '){	// part of the score matrix
						matrix[A] = parseRow(line);
					} else {	// part of the affine gap function parameters
						this.a = parseParam(line);
					}
					break;
				case 'T':
					matrix[T] = parseRow(line);
					break;
				case 'G':
					matrix[G] = parseRow(line);
					break;
				case 'C':
					matrix[C] = parseRow(line);
					break;
				case 'U':
					matrix[U] = parseRow(line);
					break;
				case 'N':
					matrix[N] = parseRow(line);
					break;
				case '*':
					matrix[SPACE] = parseRow(line);
					break;	
				case 'B':
					this.b = parseParam(line);
					break;
			}
		}
		br.close();
		
		this.matrix = matrix;
	}
	
	/**
	 * this function converts chars to ints so the program will know which matrix cell to read data from
	 * @param c the char to convert
	 * @return the int value assigned to c in ScoreMatrix class
	 */
	private int convertCharToInt(char c) {
	    int res = -100;
	    switch (c) {
	        case 'A' : res= A;   break;
	        case 'T' : res= T;   break;
	        case 'G' : res= G;   break;
	        case 'C' : res= C;   break;
	        case 'N' : res= N;   break;
	        case 'U' : res= U;   break;
	        case '*' : res= SPACE;   break;
	    }
	    return res;
	}
	
	/**
	 * this function returns the score of an alignment of two chars
	 * @param c1 the first aligned char
	 * @param c2 the second aligned char
	 * @return the score matrix value of the alignment of the chars
	 */
	public int getScore (char c1, char c2){
		return this.matrix[convertCharToInt(c1)][convertCharToInt(c2)];
	}
	
	/**
	 * this function returns the 'a' parameter of the affine function 
	 * @return the a parameter parsed from the score matrix that was given
	 */
	public int getAVal(){
		return this.a;
	}
	
	/**
	 * this function returns the 'b' parameter of the affine function
	 * @return the 'b' parameter parsed from the score matrix that was given
	 */
	public int getBVal(){
		return this.b;
	}
}