package SequenceAlignment;

public class Cell {
	/**
	 * the previous cell in the alignment matrix
	 */
	private Cell prev;
	
	/**
	 * the score of the best path to this Cell
	 */
	private double score;
	
	/**
	 * the row index of this Cell
	 */
	private int row;
	
	/**
	 * the column index of this Cell
	 */
	private int col;

	// Constructors
	public Cell() {
		prev = null;
		score = 0;
		row = 0;
		col = 0;
	}

	/**
	 * @param s
	 *            is the score
	 * @param r
	 *            is the row
	 * @param c
	 *            is the column
	 * @param p
	 *            is the previous Cell
	 */
	public Cell(int r, int c, double s, Cell p) {
		prev = p;
		score = s;
		row = r;
		col = c;
	}

	/**
	 * 
	 * @param r
	 *            is the row
	 * @param c
	 *            is the column
	 * @param p
	 *            is the previous Cell
	 */
	public Cell(int r, int c, Cell p) {
		prev = p;
		score = 0;
		row = r;
		col = c;
	}
	
	/**
	 * @param s
	 *            is the score
	 * @param r
	 *            is 0
	 * @param c
	 *            is 0
	 * @param p
	 *            is the previous Cell
	 */
	public Cell(double s,Cell p) {
		prev = p;
		score = s;
		row = 0;
		col = 0;
	}
	
	public Cell(Cell other){
		this.prev = other.prev;
		this.score = other.score;
		this.row = other.row;
		this.col = other.col;
	}
	
	// getters
	
	/**
	 * @return this Cell' row index
	 */
	public int getRow(){
		return this.row;
	}
	
	/**
	 * @return this Cell' column index
	 */
	public int getCol(){
		return this.col;
	}
	
	/**
	 * @return this Cell' score
	 */
	public double getScore(){
		return this.score;
	}
	
	/**
	 * @return this Cell' previous Cell
	 */
	public Cell getPrev(){
		return this.prev;
	}
	
	//Setters
	/**
	 * Sets this Cell' row index
	 * @param r row index
	 */
	public void setRow(int r){
		this.row =r;
	}
	
	/**
	 * Sets this Cell' column index
	 * @param c column index
	 */
	public void setCol(int c){
		this.col =c;
	}
	
	/**
	 * Sets this Cell' score
	 * @param s score
	 */
	public void setScore(double s){
		this.score =s;
	}
	
	/**
	 * Sets this Cell previous Cell
	 * @param p a Cell
	 */
	public void setPrev(Cell p){
		this.prev =p;
	}
	
}