package SequenceAlignment;

import java.io.FileNotFoundException;
import java.io.IOException;

 
public class Main {

	static boolean global = false;
	static boolean local = false;
	static boolean gap = false;
	static boolean affine = false;
	
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {

		int i;
		System.out.print("Command:\njava -jar Alignments.jar ");
		for (i = 0; i < args.length; i++){
			System.out.print(args[i]+" ");
		}
		System.out.println();
		
		//Initialization
		i = args.length-1;
		String s2 = args[i];
		i--;
		String s1 = args[i];
		i--;
		ScoreMatrix matrix = new ScoreMatrix();
		matrix.ParseFile(args[i]);
		i--;
		if (i == 1){
			if (args[i].compareTo("-p") == 0){
				gap = true;
			} else {
				affine = true;
			}
		} 
		if (args[0].compareTo("-g") == 0){
			global = true;
		} else {
			local = true;
		}
		
		Alignment alignment = new Alignment(s1, s2, matrix);
		// end of initialization
		double score = 0;
		System.out.println("Output:");
		if (global){
			if (gap){ // Global alignment with gap
				score = alignment.globalAlignmentWithGaps();
			} else if (affine){ // Global alignment with affine gap
				score = alignment.globalAlignmentAffine();
			} else { // Global alignment
				score = alignment.globalAlignment();
			}
		} else {
			if (gap){ // Local alignment with gap
				score = alignment.localAlignmentWithGaps();
			} else if (affine){ // Local alignment with affine gap
				score = alignment.localAlignmentAffine();
			} else { // Local alignment
				score = alignment.locaAlignment();
			}
		}
		System.out.println("Score: "+score);
	} // main
} // Class