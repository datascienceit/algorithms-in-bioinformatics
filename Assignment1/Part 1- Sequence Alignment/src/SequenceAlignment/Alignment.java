package SequenceAlignment;

public class Alignment {
	private String s1;
	private String s2;
	private STATUS status;
	private ScoreMatrix scoreMatrix;
	private Cell[][] alignmentMatrix;
	private int colIndOfEndCell;
	private int rowIndOfEndCell;
	private int colIndOfStartCell;
	private int rowIndOfStartCell;

	public static enum STATUS {
		NON,
		GLOBAL_ALIGNMENT_REGULAR,
		LOCAL_ALIGNMENT_REGULAR,
		GLOBAL_ALIGNMENT_WITH_GAPS,
		LOCAL_ALIGNMENT_WITH_GAPS,
		GLOBAL_ALIGNMENT_WITH_AFFINE_GAP_PENALTY,
		LOCAL_ALIGNMENT_WITH_AFFINE_GAP_PENALTY,
	}

	// Constructors
	public Alignment() {
		this.s1 = "";
		this.s2 = "";
		this.status = STATUS.NON;
		this.scoreMatrix = null;
		this.alignmentMatrix = null;
		this.colIndOfEndCell = 0;
		this.rowIndOfEndCell = 0;
		this.colIndOfStartCell = 0;
		this.rowIndOfStartCell = 0;
	}

	public Alignment(String a, String b, ScoreMatrix s) {
		this.s1 = a;
		this.s2 = b;
		this.status = STATUS.NON;
		this.scoreMatrix = s;
		this.alignmentMatrix = new Cell[s1.length() + 1][s2.length() + 1];
		this.colIndOfEndCell = 0;
		this.rowIndOfEndCell = 0;
		this.colIndOfStartCell = 0;
		this.rowIndOfStartCell = 0;
	}

	// setters
	/**
	 * This function sets the first string to be aligned
	 * @param a a string
	 */
	public void setFirstString(String a) {
		this.s1 = a;
	}

	/**
	 * This function sets the second string to be aligned
	 * @param b a string
	 */
	public void setSecondString(String b) {
		this.s2 = b;
	}

	/**
	 *  This function sets the score matrix
	 * @param s score matrix
	 */
	public void setScoreMatrix(ScoreMatrix s) {
		this.scoreMatrix = s;
	}

	//Getters
	/**
	 * This function gets the alignment matrix
	 * @return Alignment matrix
	 */
	public Cell[][] getAlignmentMatrix() {
		return this.alignmentMatrix;
	}

	/**
	 * This function gets the score matrix
	 * @return Score matrix
	 */
	public ScoreMatrix getScoreMatrix() {
		return this.scoreMatrix;
	}

	/**
	 * M[i][j]=max{opt(i-1,j-1)+s(s1(i),s2(j)), opt(i-1,j)+S(s1(i),'*')),
	 * opt(i,j-1)+S('*',s2(j))}
	 * 
	 * Base case: opt(i,0)=Sigma (S(s1(i),'*'), opt(0,j)=Sigma(S('*',s2(j)))
	 * 
	 * return opt(m,n)
	 * 
	 * @return score of a global alignment
	 */
	public double globalAlignment() {
		int i = 0;// row
		int j = 0;// column
		double sum = 0;
		int rowLen = s1.length() + 1;
		int colLen = s2.length() + 1;
		
		Cell diagonal, left, above, max;

		// Initialization
		alignmentMatrix[0][0] = new Cell(0, 0, null);

		// 1st column initialization
		for (i = 1; i < rowLen; i++) {
			alignmentMatrix[i][0] = new Cell(i, 0, scoreMatrix.getScore(s1.charAt(i - 1), '*') + sum, alignmentMatrix[i - 1][0]);
			sum = alignmentMatrix[i][0].getScore();
		}

		sum = 0;

		// 1st row initialization
		for (j = 1; j < colLen; j++) {
			alignmentMatrix[0][j] = new Cell(0, j, scoreMatrix.getScore('*', s2.charAt(j - 1)) + sum, alignmentMatrix[0][j - 1]);
			sum = alignmentMatrix[0][j].getScore();
		}

		// main loop (filling the Opt matrix)
		for (i = 1; i < rowLen; i++) {
			for (j = 1; j < colLen; j++) {
				// (M\R)
				diagonal = new Cell(alignmentMatrix[i - 1][j - 1]);
				diagonal.setScore(scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)) + diagonal.getScore());
				// (I)
				left = new Cell(alignmentMatrix[i - 1][j]);
				left.setScore(scoreMatrix.getScore(s1.charAt(i - 1), '*') + left.getScore());
				// (D)
				above = new Cell(alignmentMatrix[i][j - 1]);
				above.setScore(scoreMatrix.getScore('*', s2.charAt(j - 1)) + above.getScore());
				// finding the max
				max = maxCell(diagonal, left, above);
				alignmentMatrix[i][j] = new Cell(i, j, max.getScore(), max);
			}
		}
		
		this.rowIndOfEndCell = rowLen - 1;
		this.colIndOfEndCell = colLen - 1;
		//printAlignmentMatrix();
		printSummary(this.alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell]);
		return alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell].getScore();
	}

	/**
	 * M[i][j]=max{opt(i-1,j-1)+S(s1(i-1),s2(j-1)),opt(i-1,j)+S(s1(i-1),'*')),
	 * opt(i,j-1)+S('*',s2(j-1)),0}
	 * 
	 * Base case: opt(i,0)=opt(0,j)=0
	 * 
	 * return max{opt(i,j)|0<=i,j & i<=s1.len & j<=s2.len}
	 * 
	 * @return score of a local alignment
	 */
	public double locaAlignment() {
		int i = 0;// row
		int j = 0;// column
		int rowLen = s1.length() + 1;
		int colLen = s2.length() + 1;
		double ans = 0;
		Cell diagonal, left, above, max, freeEnd;

		// Initialization

		// 1st column initialization
		for (i = 0; i < rowLen; i++) {
			alignmentMatrix[i][0] = new Cell(i, 0, 0, null);
		}

		// 1st row initialization
		for (j = 0; j < colLen; j++) {
			alignmentMatrix[0][j] = new Cell(0, j, 0, null);
		}

		// main loop
		for (i = 1; i < rowLen; i++) {
			for (j = 1; j < colLen; j++) {
				freeEnd = new Cell(i, j, null);
				// (M\R)
				diagonal = new Cell(alignmentMatrix[i - 1][j - 1]);
				diagonal.setScore(scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)) + diagonal.getScore());
				// (D)
				left = new Cell(alignmentMatrix[i - 1][j]);
				left.setScore(scoreMatrix.getScore(s1.charAt(i - 1), '*') + left.getScore());
				// (I)
				above = new Cell(alignmentMatrix[i][j - 1]);
				above.setScore(scoreMatrix.getScore('*', s2.charAt(j - 1)) + above.getScore());
				// finding the max
				max = maxCell4(diagonal, left, above, freeEnd);
				alignmentMatrix[i][j] = new Cell(i, j, max.getScore(), max);

				if (alignmentMatrix[i][j].getScore() > ans) {// save max of all i,j
					ans = alignmentMatrix[i][j].getScore();
					this.rowIndOfEndCell = i;
					this.colIndOfEndCell = j;
				}
			}
		}
		
		//printAlignmentMatrix();
		printSummary(this.alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell]);
		return ans;
	}

	/**
	 * This function calculate a gap penalty according to the length of the gap
	 * 
	 * @param gapLength the length of the gap
	 * @return the (score) penalty of the gap
	 */
	private double gapPenalty(int gapLength) {
		return -10 + Math.log(gapLength);
	}

	/**
	 * M[i][j]=max{opt(i-1,j-1)+s(s1(i),s2(j)), max{opt(i-k,j)-w(k) : 1<=k<=i},
	 * max{opt(i,j-k)-w(k) : 1<=k<=j}
	 * 
	 * Base case: opt(i,0)=max{ opt(i-k,0) - w(k): 1<=k<=i, opt(0,j)=max{
	 * opt(0,j-k) - w(k): 1<=k<=j
	 * 
	 * return opt(m,n)
	 * 
	 * @return score of a global alignment with gaps
	 */
	public double globalAlignmentWithGaps() {
		int i = 0;// row
		int j = 0;// column
		double penalty = 0;
		double maxValue;
		int rowLen = s1.length() + 1;
		int colLen = s2.length() + 1;
		
		Cell diagonal, left, above, maxCell;

		// Initialization
		alignmentMatrix[0][0] = new Cell(0, 0, null);

		// 1st column initialization
		maxValue = Integer.MIN_VALUE;
		for (i = 1; i < rowLen; i++) {
			maxCell = alignmentMatrix[0][0];
			for (int k = 1; k <= i; k++) {
				if (maxValue < alignmentMatrix[i - k][0].getScore() + gapPenalty(k)) {
					maxCell = alignmentMatrix[i - k][0];
					penalty = gapPenalty(k);
					maxValue = alignmentMatrix[i - k][0].getScore() + penalty;
				}
			}
			alignmentMatrix[i][0] = new Cell(i, 0, maxCell.getScore() + penalty, maxCell);
		}

		// 1st row initialization
		maxValue = Integer.MIN_VALUE;
		for (j = 1; j < colLen; j++) {
			maxCell = alignmentMatrix[0][0];
			for (int k = 1; k <= j; k++) {
				if (maxValue < alignmentMatrix[0][j - k].getScore() + gapPenalty(k)) {
					maxCell = alignmentMatrix[0][j - k];
					penalty = gapPenalty(k);
					maxValue = alignmentMatrix[0][j - k].getScore() + penalty;
				}
			}
			alignmentMatrix[0][j] = new Cell(0, j, maxCell.getScore() + penalty, maxCell);
		}

		// main loop
		for (i = 1; i < rowLen; i++) {
			for (j = 1; j < colLen; j++) {
				// (M\R)
				diagonal = new Cell(alignmentMatrix[i - 1][j - 1]);
				diagonal.setScore(scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)) + diagonal.getScore());
				// (I)
				maxValue = Integer.MIN_VALUE;
				maxCell = alignmentMatrix[0][j];
				for (int k = 1; k <= i; k++) {
					if (maxValue < alignmentMatrix[i - k][j].getScore() + gapPenalty(k)) {
						maxCell = alignmentMatrix[i - k][j];
						maxValue = alignmentMatrix[i - k][j].getScore() + gapPenalty(k);
					}
				}
				// save the best Cell from the left
				left = new Cell(maxCell);
				left.setScore(maxValue);

				// (D)
				maxValue = Integer.MIN_VALUE;
				maxCell = alignmentMatrix[0][0];
				for (int k = 1; k <= j; k++) {
					if (maxValue < alignmentMatrix[i][j - k].getScore() + gapPenalty(k)) {
						maxCell = alignmentMatrix[i][j - k];
						maxValue = alignmentMatrix[i][j - k].getScore() + gapPenalty(k);
					}
				}
				// save the best Cell from above
				above = new Cell(maxCell);
				above.setScore(maxValue);

				// finding the max
				maxCell = maxCell(diagonal, left, above);
				alignmentMatrix[i][j] = new Cell(i, j, maxCell.getScore(), maxCell);
			}
		}
		
		this.rowIndOfEndCell = rowLen - 1;
		this.colIndOfEndCell = colLen - 1;
		//printAlignmentMatrix();
		printSummary(this.alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell]);

		return alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell].getScore();
	}

	/**
	 * M[i][j]=max{0, opt(i-1,j-1)+s(s1(i),s2(j)), max{opt(i-k,j)-w(k) |
	 * 1<=k<=i}, max{opt(i,j-k)-w(k) | 1<=k<=j} }
	 * 
	 * Base case: opt(i,0)=opt(0,j)=0
	 * 
	 * return max{opt(i,j)|0<=i<=s1.length, 0<=j<=s2.length}
	 * 
	 * @return score of a local alignment with gaps
	 */
	public double localAlignmentWithGaps() {
		int i = 0;// row
		int j = 0;// column
		double maxValue;
		int rowLen = s1.length() + 1;
		int colLen = s2.length() + 1;
		double ans = 0;

		Cell diagonal, left, above, maxCell, freeEnd;

		// Initialization
		alignmentMatrix[0][0] = new Cell(0, 0, null);

		// 1st column initialization
		for (i = 0; i < rowLen; i++) {
			alignmentMatrix[i][0] = new Cell(i, 0, 0, null);
		}

		// 1st row initialization
		for (j = 0; j < colLen; j++) {
			alignmentMatrix[0][j] = new Cell(0, j, 0, null);
		}

		// main loop
		for (i = 1; i < rowLen; i++) {
			for (j = 1; j < colLen; j++) {
				freeEnd = new Cell(i, j, null);
				// (M\R)
				diagonal = new Cell(alignmentMatrix[i - 1][j - 1]);
				diagonal.setScore(scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)) + diagonal.getScore());
				// (I)
				maxValue = Integer.MIN_VALUE;
				maxCell = alignmentMatrix[0][j];
				for (int k = 1; k <= i; k++) {
					if (maxValue < alignmentMatrix[i - k][j].getScore() + gapPenalty(k)) {
						maxCell = alignmentMatrix[i - k][j];
						maxValue = alignmentMatrix[i - k][j].getScore() + gapPenalty(k);
					}
				}
				left = new Cell(maxCell);
				left.setScore(maxValue);
				// (D)
				maxValue = Integer.MIN_VALUE;
				maxCell = alignmentMatrix[0][0];
				for (int k = 1; k <= j; k++) {
					if (maxValue < alignmentMatrix[i][j - k].getScore() + gapPenalty(k)) {
						maxCell = alignmentMatrix[i][j - k];
						maxValue = alignmentMatrix[i][j - k].getScore() + gapPenalty(k);
					}
				}
				above = new Cell(maxCell);
				above.setScore(maxValue);


				maxCell = maxCell4(diagonal, left, above, freeEnd);
				alignmentMatrix[i][j] = new Cell(i, j, maxCell.getScore(), maxCell);

				if (alignmentMatrix[i][j].getScore() > ans) {// save max of all i,j
					ans = alignmentMatrix[i][j].getScore();
					this.rowIndOfEndCell = i;
					this.colIndOfEndCell = j;
				}
			}
		}
		
		//printAlignmentMatrix();
		printSummary(this.alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell]);
		return ans;
	}

	/**
	 * M[i][j] - best score given that S1(i) is aligned to S2(j) 
	 * X[i][j] - best score given that S1(i) is aligned to a gap 
	 * Y[i][j] - best score given that S2(j) is aligned to a gap
	 * 
	 * M[i][j] = max{ M(i-1,j-1)+s(S1(i),S2(j)), X(i-1,j-1)+s(S1(i),S2(j)),Y(i-1,j-1)+s(S1(i),S2(j)) } 
	 * X[i][j] = max{ M(i-1,j)+a+b, X(i-1,j)+a }
	 * Y[i][j] = max{ M(i,j-1)+a+b, Y(i,j-1)+a }
	 * 
	 * Base case: M[0,0] = 0, M[i][0] = M[0][j] = -infinity for i,j>0 X[i,0] =
	 * -b -a*i for i>=0, X[0,j] -infinity for j>0 F[0,j] = -b -a*j for j>=0,
	 * Y[i,0] -infinity for i>0
	 * 
	 * return max{ M(m,n), X(m,n), Y(m,n) }
	 * 
	 * @return score of a global alignment with affine gap weights
	 */
	public double globalAlignmentAffine() {
		int i = 0;// row
		int j = 0;// column
		int a = this.scoreMatrix.getAVal();
		int b = this.scoreMatrix.getBVal();
		int rowLen = s1.length() + 1;
		int colLen = s2.length() + 1;

		Cell diagonal, left, above, maxCell;

		Cell[][] X = new Cell[rowLen][colLen];
		Cell[][] Y = new Cell[rowLen][colLen];

		// Initialization
		this.alignmentMatrix[0][0] = new Cell(0, 0, null);
		X[0][0] = new Cell(0, 0, -b - 0 * a, null);
		Y[0][0] = new Cell(0, 0, -b - 0 * a, null);

		// 1st column initialization
		for (i = 1; i < rowLen; i++) {
			this.alignmentMatrix[i][0] = new Cell(i, 0, Integer.MIN_VALUE,
					this.alignmentMatrix[i - 1][0]);
			X[i][0] = new Cell(i, 0, -b - i * a, X[i - 1][0]);
			Y[i][0] = new Cell(i, 0, Integer.MIN_VALUE, Y[i - 1][0]);
		}

		// 1st row initialization
		for (j = 1; j < colLen; j++) {
			this.alignmentMatrix[0][j] = new Cell(0, j, Integer.MIN_VALUE,
					this.alignmentMatrix[0][j - 1]);
			Y[0][j] = new Cell(0, j, -b - j * a, Y[0][j - 1]);
			X[0][j] = new Cell(0, j, Integer.MIN_VALUE, X[0][j - 1]);
		}

		// main loop
		for (i = 1; i < rowLen; i++) {
			for (j = 1; j < colLen; j++) {

				// (M\R)
				diagonal = new Cell(this.alignmentMatrix[i - 1][j - 1]);
				diagonal.setScore(this.scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)) + diagonal.getScore());

				// (I)
				if (this.alignmentMatrix[i - 1][j].getScore() - a - b >= X[i - 1][j].getScore() - a) {
					X[i][j] = new Cell(this.alignmentMatrix[i - 1][j]);
					X[i][j].setScore(this.alignmentMatrix[i - 1][j].getScore() - a - b);
				} else {
					X[i][j] = new Cell(X[i - 1][j]);
					X[i][j].setScore(X[i - 1][j].getScore() - a);
				}

				// (D)
				if (this.alignmentMatrix[i][j - 1].getScore() - a - b >= Y[i][j - 1].getScore() - a) {
					Y[i][j] = new Cell(this.alignmentMatrix[i][j - 1]);
					Y[i][j].setScore(this.alignmentMatrix[i][j - 1].getScore() - a - b);
				} else {
					Y[i][j] = new Cell(Y[i][j - 1]);
					Y[i][j].setScore(Y[i][j - 1].getScore() - a);
				}

				left = new Cell(i - 1, j - 1, X[i - 1][j - 1].getScore() + this.scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)), X[i - 1][j - 1].getPrev());
				above = new Cell(i - 1, j - 1, Y[i - 1][j - 1].getScore() + this.scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)), Y[i - 1][j - 1].getPrev());
				// finding the max
				maxCell = maxCell(diagonal, left, above);
				alignmentMatrix[i][j] = new Cell(i, j, maxCell.getScore(), maxCell);
			}
		}
		this.rowIndOfEndCell = rowLen - 1;
		this.colIndOfEndCell = colLen - 1;
		
		maxCell = maxCell(
				this.alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell],
				X[this.rowIndOfEndCell][this.colIndOfEndCell],
				Y[this.rowIndOfEndCell][this.colIndOfEndCell]);

		//printAlignmentMatrix();		
		printSummary(maxCell);
		return alignmentMatrix[rowIndOfEndCell][colIndOfEndCell].getScore();
	}

	/**
	 * M[i][j] - best score given that S1(i) is aligned to S2(j) X[i][j] - best
	 * score given that S1(i) is aligned to a gap Y[i][j] - best score given
	 * that S2(j) is aligned to a gap
	 * 
	 * M[i][j] = max{ 0, M(i-1,j-1)+s(S1(i),S2(j)), X(i-1,j-1)+s(S1(i),S2(j)),
	 * Y(i-1,j-1)+s(S1(i),S2(j)) } X[i][j] = max{ M(i-1,j)+a+b, X(i-1,j)+a }
	 * Y[i][j] = max{ M(i,j-1)+a+b, Y(i,j-1)+a }
	 * 
	 * Base case: M[0,0] = 0, M[i][0] = M[0][j] = -infinity for i,j>0 X[i,0] =
	 * -b -a*i for i>=0, X[0,j] -infinity for j>0 F[0,j] = -b -a*j for j>=0,
	 * Y[i,0] -infinity for i>0
	 * 
	 * return max{ M(m,n), X(m,n), Y(m,n) }
	 * 
	 * @return score of a local alignment with affine gap weights
	 */
	public double localAlignmentAffine() {
		int i = 0;// row
		int j = 0;// column
		int a = this.scoreMatrix.getAVal();
		int b = this.scoreMatrix.getBVal();
		int rowLen = s1.length() + 1;
		int colLen = s2.length() + 1;
		double ans = Integer.MIN_VALUE;

		Cell diagonal, left, above, maxCell, freeEnd;

		Cell[][] X = new Cell[rowLen][colLen];
		Cell[][] Y = new Cell[rowLen][colLen];

		// Initialization
		this.alignmentMatrix[0][0] = new Cell(0, 0, null);
		X[0][0] = new Cell(0, 0, Integer.MIN_VALUE, null);
		Y[0][0] = new Cell(0, 0, Integer.MIN_VALUE, null);

		// 1st column initialization
		for (i = 1; i < rowLen; i++) {
			this.alignmentMatrix[i][0] = new Cell(i, 0, 0, this.alignmentMatrix[i - 1][0]);
			X[i][0] = new Cell(i, 0, Integer.MIN_VALUE, X[i - 1][0]);
			Y[i][0] = new Cell(i, 0, Integer.MIN_VALUE, Y[i - 1][0]);
		}

		// 1st row initialization
		for (j = 1; j < colLen; j++) {
			this.alignmentMatrix[0][j] = new Cell(0, j, 0, this.alignmentMatrix[0][j - 1]);
			Y[0][j] = new Cell(0, j, Integer.MIN_VALUE, Y[0][j - 1]);
			X[0][j] = new Cell(0, j, Integer.MIN_VALUE, X[0][j - 1]);
		}

		// main loop (filling the Opt matrix)
		for (i = 1; i < rowLen; i++) {
			for (j = 1; j < colLen; j++) {

				freeEnd = new Cell(i, j, null);

				// (M\R)
				diagonal = new Cell(this.alignmentMatrix[i - 1][j - 1]);
				diagonal.setScore(this.scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)) + diagonal.getScore());

				// (I)
				if (this.alignmentMatrix[i - 1][j].getScore() - a - b >= X[i - 1][j].getScore() - a) {
					X[i][j] = new Cell(this.alignmentMatrix[i - 1][j]);
					X[i][j].setScore(this.alignmentMatrix[i - 1][j].getScore() - a - b);
				} else {
					X[i][j] = new Cell(X[i - 1][j]);
					X[i][j].setScore(X[i - 1][j].getScore() - a);
				}

				// (D)
				if (this.alignmentMatrix[i][j - 1].getScore() - a - b >= Y[i][j - 1].getScore() - a) {
					Y[i][j] = new Cell(this.alignmentMatrix[i][j - 1]);
					Y[i][j].setScore(this.alignmentMatrix[i][j - 1].getScore() - a - b);
				} else {
					Y[i][j] = new Cell(Y[i][j - 1]);
					Y[i][j].setScore(Y[i][j - 1].getScore() - a);
				}

				left = new Cell(i - 1, j - 1, X[i - 1][j - 1].getScore() + this.scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)), X[i - 1][j - 1].getPrev());
				above = new Cell(i - 1, j - 1, Y[i - 1][j - 1].getScore() + this.scoreMatrix.getScore(s1.charAt(i - 1), s2.charAt(j - 1)), Y[i - 1][j - 1].getPrev());
				
				// finding the max
				maxCell = maxCell4(freeEnd, diagonal, left, above);
				alignmentMatrix[i][j] = new Cell(i, j, maxCell.getScore(), maxCell);

				// updating indices of the cell with the top score
				maxCell = maxCell(this.alignmentMatrix[i][j], X[i][j], Y[i][j]);
				if (maxCell.getScore() >= ans) {
					ans = maxCell.getScore();
					this.rowIndOfEndCell = i;
					this.colIndOfEndCell = j;
				}
			}
		}

		//printAlignmentMatrix();
		printSummary(this.alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell]);

		return this.alignmentMatrix[this.rowIndOfEndCell][this.colIndOfEndCell].getScore();
	}
	
	/**
	 * 
	 * @param i row index from which to start the traceback from
	 * @param j column index from which to start the traceback from
	 * @param c the cell from which to start the traceback from
	 * @return string composed of M/R/I/D representing the alignment
	 */
	public String getTraceback(int i, int j, Cell c) {

		String ans = "";
		Cell currentCell = c;

		Cell prev = currentCell.getPrev();
		int prevRow, prevCol;

		while (prev != null) {
			prevRow = prev.getRow();
			prevCol = prev.getCol();
			if (prevRow == i - 1 && prevCol == j - 1) { // M\R
				if (s1.charAt(i - 1) == (s2.charAt(j - 1))) {// (M)

					ans = "M" + ans;
				} else {// (R)

					ans = "R" + ans;
				}
			} else if (prevRow <= i - 1 && prevCol == j) { // one (deletion) or more (gap) 'D's
				for (int k = prevRow; k < i; k++) {
					ans = "D" + ans;
				}
			} else /* prevRow == i - 1 && prevCol < j */{ // one (insertion) or more (gap) 'I's
				for (int k = prevCol; k < j; k++) {
					ans = "I" + ans;
				}
			}

			prev = prev.getPrev();

			i = prevRow;
			j = prevCol;
		}
		this.rowIndOfStartCell = i;
		this.colIndOfStartCell = j;
		
		//printStartAndEndCells();
		
		return ans;
	}

	/**
	 * prints the aligned strings
	 * @param alignment string composed of M/R/I/D representing the alignment
	 * @param endRow row index from which to start
	 * @param endCol column index from which to start
	 */
	public void printAlignment(String alignment, int endRow, int endCol) {
		int i = endRow;
		int j = endCol;
		String row1 = "";
		String row2 = "";
		while (alignment.length() > 0) {
			switch (alignment.charAt(alignment.length() - 1)) {

			case 'M':
			case 'R':
				row1 = s1.charAt(i - 1) + row1;
				row2 = s2.charAt(j - 1) + row2;
				i--;
				j--;
				break;

			case 'I':
				row1 = '_' + row1;
				row2 = s2.charAt(j - 1) + row2;
				j--;

				break;

			case 'D':
				row1 = s1.charAt(i - 1) + row1;
				row2 = '_' + row2;
				i--;
				break;
			}
			alignment = alignment.substring(0, alignment.length() - 1);
		}

		System.out.println(row1);
		System.out.println(row2);
	}

	/**
	 * This function returns the cell with the largest score, out of three
	 * @param a a cell
	 * @param b a cell
	 * @param c a cell
	 * @return the cell with the largest score
	 */
	private Cell maxCell(Cell a, Cell b, Cell c) {
		if (a.getScore() >= b.getScore()) {
			if (a.getScore() >= c.getScore()) {
				return a;
			} else {
				return c;
			}
		} else if (b.getScore() >= c.getScore()) {
			return b;
		} else {
			return c;
		}
	}

	/**
	 * This function returns the cell with the largest score, out of four
	 * @param a a cell
	 * @param b a cell
	 * @param c a cell
	 * @param d a cell
	 * @return the cell with the largest score
	 */
	private Cell maxCell4(Cell a, Cell b, Cell c, Cell d) {
		if (a.getScore() >= b.getScore()) {
			if (a.getScore() >= c.getScore()) {
				if (a.getScore() >= d.getScore()) {
					return a;
				} else {
					return d;
				}
			} else if (c.getScore() >= d.getScore()) {
				return c;
			} else {
				return d;
			}
		} else if (b.getScore() >= c.getScore()) {
			if (b.getScore() >= d.getScore()) {
				return b;
			} else {
				return d;
			}
		} else if (c.getScore() >= d.getScore()) {
			return c;
		} else {
			return d;
		}
	}

	/**
	 * This function prints the complete alignment matrix
	 */
	private void printAlignmentMatrix(){
		for (int a = 0; a <= this.s1.length(); a++) {
			for (int b = 0; b <= this.s2.length(); b++) {
				System.out.print(String.format("%.3f", this.alignmentMatrix[a][b].getScore()) + '\t');
			}
			System.out.println();
		}
	}
	
	/**
	 * This function prints a summary of the alignment
	 * @param cell a cell to start gather data from
	 */
	private void printSummary(Cell cell) {
		String tracebackString = getTraceback(this.rowIndOfEndCell,	this.colIndOfEndCell, cell);
		printAlignment(tracebackString, this.rowIndOfEndCell, this.colIndOfEndCell);
	}
	
	/**
	 * This function prints the start and end cells of the alignment
	 */
	private void printStartAndEndCells(){
		System.out.println(
				"Start Cell: (" + this.rowIndOfStartCell + "," + this.colIndOfStartCell + ")" + '\n' + 
				"End Cell: (" + this.rowIndOfEndCell + "," + this.colIndOfEndCell + ")");
	}
}