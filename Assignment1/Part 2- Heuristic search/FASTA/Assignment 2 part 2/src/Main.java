import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Vector;


public class Main {


	public static void main(String[] args) {
		int labels = -1;
		int clusters = -1;
		HashMap<String, Vector<String>> hash_labels = new HashMap<String, Vector<String>>();
		ScoringMatrix M_matrix = new ScoringMatrix(args[2]);
		Vector<String> RNASequences = new Vector<String>();
		Vector<String> headline = new Vector<String>();
		Vector<String> Names_of_labels = new Vector<String>();
		Vector<String> RNAName = new Vector<String>();
		Vector<Vector<List_score>> hierarchial_Tree = new Vector<Vector<List_score>>();
		Vector<List_score> distance_matrix = new Vector<List_score>();
		Vector<Node> root_cluster = new Vector<Node>();

		try
		{
			FileInputStream rna = new FileInputStream(args[0]);
			DataInputStream in = new DataInputStream(rna);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			String temp = "";
			while ((strLine = br.readLine()) != null)
			{
				if (!(strLine.startsWith(">")))
				{
					temp = temp +strLine;
				}
				else
				{
					String params[] = strLine.split("\\s+");
					RNAName.add(params[0].substring(1));
					RNASequences.add(temp);
					headline.add(strLine);
					temp = "";
				}

			}
			RNASequences.remove(0);
			RNASequences.add(temp);
		}
		catch (Exception e) {
			System.err.println("Error openning RNASequences " + e.getMessage());
		}


		try
		{
			FileInputStream rna = new FileInputStream(args[1]);
			DataInputStream in = new DataInputStream(rna);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null)
			{
				if (!(strLine.startsWith("/")) && !(strLine.length()==0) && !(strLine.startsWith(" ")))
				{
					Names_of_labels.add(strLine);
				}
			}
		}
		catch (Exception e) {
			System.err.println("Error openning labels " + e.getMessage());
		}
		labels = Integer.parseInt(Names_of_labels.firstElement());
		Names_of_labels.removeElementAt(0);
		clusters = Integer.parseInt(Names_of_labels.lastElement());
		Names_of_labels.removeElementAt(Names_of_labels.size()-1);
		for(int i=0; i<Names_of_labels.size(); i++)
		{
			hash_labels.put(Names_of_labels.elementAt(i), new Vector<String>());
			for(int j =0; j<headline.size(); j++)
			{
				if(headline.elementAt(j).contains(Names_of_labels.elementAt(i)))
				{
					hash_labels.get(Names_of_labels.elementAt(i)).add(RNAName.elementAt(j));
				}
			}
		}

		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter("QTEMP_output.html"));
			bw.write("<html><font size=\"5\" face='Lucida Console'><body><center><h2>Part I: Hierarchical Tree Construction</h2></center><dl>");

			String ans = "";
			BuildMatrix.BuildMatrix(RNASequences,RNAName, M_matrix, distance_matrix, bw);
			HierarchialTree.BuildHierarchialTree(RNAName,distance_matrix, hierarchial_Tree, bw);
			//HierarchialTree.PrintHierarchialTreeop(hierarchial_Tree, bw);
			Tree tree = new Tree(hierarchial_Tree, bw);
			tree.build_up(hierarchial_Tree, clusters,labels, hash_labels, Names_of_labels, RNAName, bw);
			ans = tree.root.getName();
			bw.write("<dt>Hierarchical tree:</dt></br>");
			bw.write("<br/>");
			System.out.println(ans);
			bw.write("<dt>" +ans+ "</dt>");
			bw.write("<br/>");
			bw.write("<html><font size=\"5\" face='Lucida Console'><body><center><h2>Part II: Minimum Misclassified leaves Clustering</h2></center><dl>");
			Tree.find_cluster(tree.root, root_cluster, (clusters-1), tree.root.minScore[clusters-1], Names_of_labels, bw);


			bw.write("</body></html>");
			bw.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
}
