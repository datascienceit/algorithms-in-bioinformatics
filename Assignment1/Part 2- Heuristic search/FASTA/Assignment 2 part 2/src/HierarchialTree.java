import java.awt.geom.Arc2D.Double;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Vector;

import sun.misc.Sort;


public class HierarchialTree {

	public static  void BuildHierarchialTree(Vector<String> RNAName,Vector<List_score> distance , Vector<Vector<List_score>> hierarchial_Tree, BufferedWriter bw)
	{
		Vector<List_score> temp = distance;
		boolean stop = true;
		while(stop)
		{
			add_level(hierarchial_Tree, temp);
			Collections.sort(temp);
			//			BuildMatrix.printMatrix(temp);
			if(temp.size()==1)
			{
				stop = false;
				break;
			}
			List_score temp1 = temp.elementAt(0);
			List_score temp2 = temp.elementAt(1);
			if(temp.size()==2)
			{
				List_score insert = new List_score(temp1.name+"<>"+temp2.name, Math.min(temp1.index_name,temp2.index_name),  Math.max(temp1.max,temp2.max), -1);
				insert.max = Math.max(temp.elementAt(0).max, temp.elementAt(1).max);
				temp.removeAllElements();
				temp.add(insert);
			}
			else
			{
				List_score insert = new List_score(temp1.name+"<>"+temp2.name,Math.min(temp1.index_name,temp2.index_name), Integer.MIN_VALUE, -1);
				boolean found = false;
				int found_index=-1;
				for(int i=2; i<temp.size(); i++)
				{
					int flag = 0;
					for(int j=0; j<temp.elementAt(i).scores.size(); j++)
					{
						if(j == temp1.index_max  || j == temp2.index_max)
						{
							if(flag==0)
							{
								//linkage linkage linkage
								temp.elementAt(i).scores.setElementAt(Math.min(temp.elementAt(i).scores.elementAt(temp1.index_max), temp.elementAt(i).scores.elementAt(temp2.index_max)), j);
								flag = 1;
							}
							else
							{
								found_index = j;
							}
						}
					}
					temp.elementAt(i).scores.remove(found_index);
					found_index=-1;
					flag=0;
				}
				for(int j=0; j<temp1.scores.size(); j++)
				{
					if(j!=temp1.index_max && j!=temp2.index_max)
					{
						//   linkage linkage    linkage linkage    linkage linkage
						insert.scores.add(Math.min(temp1.scores.elementAt(j), temp2.scores.elementAt(j)));
					}
					else
					{
						if(!found)
						{
							insert.scores.add((double) Integer.MIN_VALUE);
							found = true;
						}
					}
				}
				temp.remove(temp1);
				temp.remove(temp2);
				temp.add(insert);
				update(temp);
			}
		}
	}



	private static void update(Vector<List_score> temp)
	{
		for(int i=0; i<temp.size(); i++)
		{
			temp.elementAt(i).max = Integer.MIN_VALUE;
			temp.elementAt(i).index_max = -1;
			for(int j=0; j<temp.elementAt(i).scores.size(); j++)
			{
				if(temp.elementAt(i).max < temp.elementAt(i).scores.elementAt(j))
				{
					temp.elementAt(i).max = temp.elementAt(i).scores.elementAt(j);
					temp.elementAt(i).index_max = j;
				}
			}
		}
	}




	private static void add_level(Vector<Vector<List_score>> hierarchial_Tree, Vector<List_score> temp)
	{
		Vector<List_score> add = new Vector<List_score>();
		for(int i=0; i<temp.size(); i++)
		{
			List_score temp_List_score = new List_score(temp.elementAt(i).name, temp.elementAt(i).index_name, temp.elementAt(i).max, temp.elementAt(i).index_max, temp.elementAt(i).scores);
			add.add(temp_List_score);
		}
		hierarchial_Tree.add(add);
	}


	public static void PrintHierarchialTreeop(Vector<Vector<List_score>> print_lable, BufferedWriter bw)
	{
		try
		{
			for(int i=print_lable.size()-1; i>=0;  i--)
			{
				Collections.sort(print_lable.elementAt(i));
				System.out.print("label: "+i+ "   " + "max score: "+ print_lable.elementAt(i).elementAt(0).max + "   ");
				bw.write("<dt>> label: &nbsp;"+i+ "&nbsp;&nbsp;&nbsp;max score: &nbsp;"+print_lable.elementAt(i).elementAt(0).max+"&nbsp;&nbsp;&nbsp;</dt>");
				for(int j=0; j<print_lable.elementAt(i).size(); j++)
				{
					if(j == print_lable.elementAt(i).size()-1)
					{
						System.out.print(print_lable.elementAt(i).elementAt(j).name);
						bw.write(print_lable.elementAt(i).elementAt(j).name);
					}
					else
					{
						System.out.print(print_lable.elementAt(i).elementAt(j).name + "_______");
						bw.write(print_lable.elementAt(i).elementAt(j).name+"_______");
					}
				}
				System.out.println();
				bw.write("<br/>");
			}
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
