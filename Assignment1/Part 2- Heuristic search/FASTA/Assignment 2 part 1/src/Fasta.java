import java.util.HashMap;
import java.util.Vector;



/**
 * static functions that operate the fasta filtration
 * @author noam and motz
 *
 */
public class Fasta {

	HashMap<String, Vector<Integer>> sextet_T;
	static final int  LENGTH = Diagonal.LENGTH;

/**
 * a constructor that initializes the hash table for the ktups
 * @param T
 * @param P
 */
	public Fasta(String T, String P)
	{
		this.sextet_T = new HashMap<String, Vector<Integer>>();
		this.setSextet(T,LENGTH);
	}

/**
 * initializes the hash table for the ktups
 * @param T
 * @param LENGTH size of ktup
 */
	public void setSextet(String T, int LENGTH)
	{
//		System.out.println("function: setSextet in Fasta");
		for(int i=0; i<=T.length()-LENGTH; i++)
		{
			String temp = T.substring(i, i+LENGTH);
			if(sextet_T.get(temp)==null)
			{
				Vector<Integer> tempVector = new Vector<Integer>();
				tempVector.add(i);
				sextet_T.put(temp,tempVector);
			}
			else
			{
				sextet_T.get(temp).add(i);
			}
		}
	}

/**
 * determines if an EST passes the filtration and if so, sends it for alignment.
 * @param ests
 * @param T
 * @param P
 * @param matrix
 * @param table
 */
	public void checkFasta(Vector<String> ests, String T, String P, ScoringMatrix matrix, AlignmentCell[][] table)
	{
//		System.out.println("function: checkFasta in Fasta");
		Est_P Est =new Est_P();
		Est.checkOrNot(this.sextet_T, T, P, LENGTH);
		if(Est.check==false)
		{
			System.out.println("low scor:"+Est.finalScoreEst+" no need to check!!");
		}
		else
		{
//			System.out.println("Est.index_I_begin "+Est.index_I_begin);
//			System.out.println("Est.index_J_begin "+Est.index_J_begin);
//			System.out.println("Est.index_I_end "+Est.index_I_end);
//			System.out.println("Est.index_J_end "+Est.index_J_end);
//			System.out.println("Est.scoreDiagonal "+Est.finalScoreEst);
			int beginChromozome = Math.max(0, Est.index_I_begin-P.length());
			int endChromozome = Math.min(Est.index_I_end+P.length(), T.length());
			int beginEst = Math.min(0, Est.index_J_begin);
			int endEst = Math.min(Est.index_J_end,P.length());
			String chromozom = T.substring(beginChromozome, endChromozome);
//			System.out.println("index chromozome:"+beginChromozome+" "+ endChromozome);
//			System.out.println("index est: "+beginEst+" "+ endEst);
			String est = P.substring(beginEst, endEst);
			Alignments.globalAlignment(est, chromozom, matrix, table);
		}
	}

}
