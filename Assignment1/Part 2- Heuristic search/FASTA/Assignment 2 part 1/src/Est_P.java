import java.util.HashMap;
import java.util.Vector;

/**
 * a representation of the EST after filter
 * @author noam and motz
 *
 */
public class Est_P {

	public int finalScoreEst;
	static final int SCORE = 19;
	int index_I_begin;
	int index_J_begin;
	int index_I_end;
	int index_J_end;
	boolean check;

	public Est_P()
	{
		this.finalScoreEst = -1;
		this.index_I_begin = 0;
		this.index_J_begin = 0;
		this.index_I_end = 0;
		this.index_J_end = 0;
		check = false;
	}






	public void checkOrNot (HashMap<String, Vector<Integer>>  hash, String T, String P, int length )
	{
//		System.out.println("function: checkOrNot in Est_P");

		Diagonal diagonl = new Diagonal();
		diagonl=Diagonal.findDiagonal(hash, T,P, length);
		this.finalScoreEst = diagonl.scoreDiagonal;
		this.index_I_begin = diagonl.index_I_begin;
		this.index_J_begin = diagonl.index_J_begin;
		this.index_I_end = diagonl.index_I_end;
		this.index_J_end = diagonl.index_J_end;

		//########################################################
//		System.out.println("ans.index_I_begin "+this.index_I_begin);
//		System.out.println("ans.index_J_begin "+this.index_J_begin);
//		System.out.println("ans.index_I_end "+this.index_I_end);
//		System.out.println("ans.index_J_end "+this.index_J_end);
//		System.out.println("ans.scoreDiagonal "+this.finalScoreEst);
		//########################################################
		if(this.finalScoreEst>=SCORE)
		{
			this.check = true;
		}
		else
			this.check = false;
	}


}
