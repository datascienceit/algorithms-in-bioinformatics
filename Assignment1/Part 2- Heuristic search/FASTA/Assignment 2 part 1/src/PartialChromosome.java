import java.util.Vector;

import org.omg.PortableInterceptor.INACTIVE;

/**
 * an implementation of a window of the chromosome. used for practical reasons since loading the
 * entire chromosome and using a dynamic programming table on it is difficult.
 * @author noam and motz
 *
 */
public class PartialChromosome implements Comparable<PartialChromosome> {

	final static int RADIUS = 5000;
	final static int OVERLAP = 1000;

	String Partial;
	int PartialScore;
	int indexBegin;
	int indexEnd;
	int indexAlignment;

/**
 * empty constructor
 */
	public PartialChromosome()
	{
		this.Partial = "";
		this.PartialScore = -1;
	}

/**
 * constructor that creates a chromosome window which bet suits the index of a match.
 * @param chromosome the chromosome divided into windows
 * @param indexMatch index of the match we are interested in
 */
	public PartialChromosome(Vector<PartialChromosome> chromosome,  int indexMatch)
	{
		int indexAlignment = chromosome.elementAt(indexMatch).indexAlignment; //  value 0<=>10000
		int begin = chromosome.elementAt(indexMatch).indexBegin;
		boolean next = chromosome.size()>indexMatch+1;
		boolean before = indexMatch!=0;
		int ThisUp = Math.min(indexAlignment+RADIUS+OVERLAP, chromosome.elementAt(indexMatch).Partial.length());
		int NextDown = Math.min(OVERLAP+RADIUS-(chromosome.elementAt(indexMatch).Partial.length()-indexAlignment),chromosome.elementAt(indexMatch+1).Partial.length());

		if(indexAlignment<5000)
		{
			if(!before)
			{
				this.Partial = chromosome.elementAt(indexMatch).Partial;
				this.indexAlignment = chromosome.elementAt(indexMatch).indexAlignment;
				this.indexBegin = chromosome.elementAt(indexMatch).indexBegin;
				this.indexEnd = chromosome.elementAt(indexMatch).indexEnd;
				this.PartialScore = chromosome.elementAt(indexMatch).PartialScore;
			}
			else
			{
				this.Partial = chromosome.elementAt(indexMatch-1).Partial.substring(chromosome.elementAt(indexMatch-1).Partial.length()-(RADIUS-indexAlignment))+chromosome.elementAt(indexMatch).Partial.substring(OVERLAP, ThisUp);
				this.indexAlignment = chromosome.elementAt(indexMatch).indexAlignment;
				this.indexBegin = chromosome.elementAt(indexMatch).indexBegin-(RADIUS-indexAlignment);
				this.indexEnd = begin+ThisUp-OVERLAP;
				this.PartialScore = chromosome.elementAt(indexMatch).PartialScore;
			}

		}
		else
		{
			if(!next)
			{
				this.Partial = chromosome.elementAt(indexMatch).Partial;
				this.indexAlignment = chromosome.elementAt(indexMatch).indexAlignment;
				this.indexBegin = chromosome.elementAt(indexMatch).indexBegin;
				this.indexEnd = chromosome.elementAt(indexMatch).indexEnd;
				this.PartialScore = chromosome.elementAt(indexMatch).PartialScore;
			}
			else
			{
				this.Partial = chromosome.elementAt(indexMatch).Partial.substring(indexAlignment-RADIUS)+chromosome.elementAt(indexMatch+1).Partial.substring(OVERLAP, NextDown);
				this.indexAlignment = chromosome.elementAt(indexMatch).indexAlignment;
				this.indexBegin = chromosome.elementAt(indexMatch).indexBegin+indexAlignment-RADIUS;
				this.indexEnd = chromosome.elementAt(indexMatch+1).indexBegin+NextDown;
				this.PartialScore = chromosome.elementAt(indexMatch).PartialScore;
			}
		}
	}


	/**
	 * compares by score
	 */
	public int compareTo(PartialChromosome o) {
		return this.PartialScore-o.PartialScore;
	}

}
