import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


/**
 * an implementation of the scoring matrix
 * @author noam and motz
 *
 */
public class ScoringMatrix {
	static final int NUM_OF_ROWS = 8; //change to 25 if working on BLOSUM and aligning proteins
	Map<String, Integer> scoringMatrix = new HashMap<String, Integer>();
	String lines []=new String[NUM_OF_ROWS];
	int lineNum=0;
	/**
	 * a constructor that reads from the input file scoring matrix and stores it on a hash table
	 * @param fileName the scoring matrix file name
	 */
	public ScoringMatrix(String fileName) {

		try
		{
			FileInputStream input = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(input);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null) // read the ests put into vector
			{
				if (!(strLine.startsWith("#")))
				{
					lines[lineNum]=strLine;
					lineNum++;

				}

			}
		}
		catch (Exception e) {
			System.err.println("Error openning scoring matrix " + e.getMessage());
		}
		//updating hash map
		String params[]=lines[0].split("\\s+");
		for (int i=1; i< NUM_OF_ROWS; i++){
			String line[]=lines[i].split("\\s+");
			for (int j=1; j<line.length;j++) {
				scoringMatrix.put(params[i]+params[j]+"", new Integer(line[j]));
			}
		}
	}

	/**
	 * returns the score of aligning two characters using the matrix
	 * @param arg the two characters for alignment as a 2 letter string
	 * @return the score
	 */
	public int score(String arg) {
		return scoringMatrix.get(arg);
	}



}
