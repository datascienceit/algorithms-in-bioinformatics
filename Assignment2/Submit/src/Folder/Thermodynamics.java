package Folder;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class Thermodynamics extends Nussinov {

	public Thermodynamics() {
		super();
	}
	
	protected double getScore(char c1, char c2, int i1, int i2){
		int ans = 0;
		switch (c1){
			case 'g':
			case 'G':
				switch (c2){
					case 'c':
					case 'C':
						ans = 3;
					break;
					case 'u':
					case 'U':
						ans = 1;
					break;
					default:
						break;
				}
				break;
			
			case 'c':
			case 'C':
				switch (c2){
					case 'g':
					case 'G':
						ans = 3;
						break;
					default:
						break;
				}
				break;
			
			case 'a':
			case 'A':
				switch (c2){
					case 'u':
					case 'U':
						ans = 2;
					break;
					default:
						break;
				}
				break;
				
			case 'u':
			case 'U':
				switch (c2){
					case 'a':
					case 'A':
						ans = 2;
						break;
					case 'g':
					case 'G':
						ans = 1;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
		return ans;
	}

	protected void run(){
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter("Output2.fasta"));
			fold(bw);
			bw.close();
		} catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
	
}
