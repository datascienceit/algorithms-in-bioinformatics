package Folder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class MutualInformation extends Nussinov {
	private HashMap<Integer, HashMap<Character, Integer>> singleFrequencies;
	private HashMap<String, HashMap<String, Integer>> mutualFrequencies;
	private HashMap<String, Double> scores;
	private int numOfPositions;
	private int numOfSequences;
	
	public MutualInformation() {
		super();
		this.singleFrequencies = new HashMap<Integer, HashMap<Character, Integer>>();
		this.mutualFrequencies = new HashMap<String, HashMap<String, Integer>>();
		this.scores = new HashMap<String, Double>();
		this.numOfPositions = 0;
		this.numOfSequences = 0;
	}
	
	
	protected void parseFile(String file) throws IOException{
		int numOfLine = 0;
		String line = "";
		String name = "";
		String sequence = "";
		
		BufferedReader bufferesReader = null;
		try { 
			bufferesReader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			bufferesReader = new BufferedReader(new FileReader(System.getProperty("user.dir") + file));
		}
		while ((line = bufferesReader.readLine()) != null) {
			numOfLine++;
			if (numOfLine == 1){
				continue;
			}
			if (line.compareTo("") != 0){
				String[] parts = line.split(" ");
				name = parts[0];
				sequence = parts[parts.length-1];
				if (super.sequences.containsKey(name)){
					super.sequences.put(name, super.sequences.get(name)+sequence);
				} else {
					super.sequences.put(name, sequence);
				} 
			}
		}
		this.numOfSequences = super.sequences.keySet().size();
		for (String seq : super.sequences.keySet()){
			this.numOfPositions = super.sequences.get(seq).length();
			break;
		}
		
	}
	
	private void updatePosition(int i){
		for (String s: super.sequences.values()){
			char c = s.charAt(i);
			if (!this.singleFrequencies.containsKey(i)){
				this.singleFrequencies.put(i, new HashMap<Character,Integer>());
			}
			if ( !((this.singleFrequencies.get(i)).containsKey(c)) ){
				(this.singleFrequencies.get(i)).put(c,1);
			} else {
				int num = (this.singleFrequencies.get(i)).get(c);
				(this.singleFrequencies.get(i)).put(c, num+1);
			}
		}
	}
	
	private void updateMutualPositions(int i, int j){
		for (String s: super.sequences.values()){
			char c1 = s.charAt(i);
			char c2 = s.charAt(j);
			String st = ""+Integer.toString(i)+','+Integer.toString(j);
			if (!this.mutualFrequencies.containsKey(st)){
				this.mutualFrequencies.put(st, new HashMap<String, Integer>());
			}
			String str = ""+c1+c2;
			if ( !((this.mutualFrequencies.get(st)).containsKey(str)) ){
				(this.mutualFrequencies.get(st)).put(str,1);
			} else {
				int num = (this.mutualFrequencies.get(st)).get(str);
				(this.mutualFrequencies.get(st)).put(str, num+1);
			}
		}
	}
	
	private void computeScores(){
		double mutFreq;
		
		for (int j = 1; j < this.numOfPositions; j++){
			for (int i = 0; i < j; i++){
				String pair = ""+Integer.toString(i)+','+Integer.toString(j);
				double score = 0;
				for (Character c1 : this.singleFrequencies.get(i).keySet()){
					for (Character c2 : this.singleFrequencies.get(j).keySet()){
						if ((((this.mutualFrequencies).get(pair)).get(""+c1+c2)) != null){
							mutFreq = ( (double)(((this.mutualFrequencies).get(pair)).get(""+c1+c2)) / (double)this.numOfSequences );
						} else {
							mutFreq = 0;
						}
						if (mutFreq > 0){
							double firstFreq = (double)this.singleFrequencies.get(i).get(c1) / this.numOfSequences;
							double secondFreq = (double)this.singleFrequencies.get(j).get(c2) / this.numOfSequences;
							double log = (Math.log(mutFreq / (firstFreq * secondFreq) )) / (Math.log(2));
							score += mutFreq * log;
						}
					}
				}
				this.scores.put(pair, score);
			}
		}
		
		BufferedWriter bw2;
		try {
			bw2 = new BufferedWriter(new FileWriter("MutInfScores.txt"));
			for (String pair : this.scores.keySet()){
				bw2.write(pair+"-"+this.scores.get(pair));
				bw2.newLine();
			}
			bw2.close();
		} catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	public void computeCovariationScores(){
		int i, j;
		
		updatePosition(0);
		
		for (j = 1; j < this.numOfPositions; j++){
			updatePosition(j);
			for (i = 0; i < j; i++){
				try {
					updateMutualPositions(i, j);
				} catch (Exception e){
					System.out.println(e.getMessage());
					System.out.println(i+","+j);
				}
			}
		}
		computeScores();
	}
	
	//return the score by this score.matrix
	protected double getScore(char c1, char c2, int i, int j){
		String pair = ""+i+','+j;
		double ans = this.scores.get(pair);
		if (okPair(c1, c2)){
			ans += 1; // +1 for regular Nussinov score
		}
		return ans;
	}
	
	protected void run(){
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter("mutualInformationConsensus.fasta"));
			fold(bw);
			bw.close();
		} catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	protected void fold(BufferedWriter bw){
		String sequence = "";
		String name = "Consensus";
		int freq, maxFrequency;
		char ch;
		try { 
			sequence = "";
			for (int i = 0 ; i < this.singleFrequencies.keySet().size(); i++){
				ch = 'a';
				maxFrequency = 0;
				for (Character c : this.singleFrequencies.get(i).keySet()){
					freq = this.singleFrequencies.get(i).get(c);
					if (freq > maxFrequency){
						ch = c;
						maxFrequency = freq;
					}
				}
				sequence += ch;
			}
			
			findMaxBasePairs(sequence);

			bw.write(">"+name);
			bw.newLine();
			bw.write(sequence);
			bw.newLine();
			System.out.println(">"+name);
			System.out.println(sequence);
			traceback(sequence);
			printTraceback(sequence, bw);
		} catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			for (StackTraceElement ste : e.getStackTrace()){
				System.out.println(ste.toString());
			}
		}
	}
}
