package Folder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;

public class Nussinov {

	protected HashMap<String, String> sequences;
	protected double[][] table;
	protected char[] traceback;

	public Nussinov(){
		this.sequences = new HashMap<String, String>();
		this.table = new double[0][0];
	}

	protected void parseFile(String file) throws IOException{
		String line = "";
		String name = "";
		String sequence = "";

		BufferedReader bufferesReader = null;
		try { 
			bufferesReader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			bufferesReader = new BufferedReader(new FileReader(System.getProperty("user.dir") + file));
		}
		while ((line = bufferesReader.readLine()) != null) {
			if (line.charAt(0) == '>') { // name of query sequence
				name = line.substring(1);
			} else {
				sequence = line.replaceAll("[^actgunACTGUN]", "");
				this.sequences.put(name, sequence);
			}
		}
	}

	protected void initializeTable(){
		int i;
		for (i = 2; i < this.table[0].length; i++){
			this.table[i][i-1] = 0;
		}
		for (i = 1; i < this.table[0].length; i++){
			this.table[i][i] = 0;
		}
	}

	protected double getScore(char c1, char c2, int i1, int i2){
		int ans = 0;
		switch (c1){
		case 'g':
		case 'G':
			switch (c2){
			case 'c':
			case 'C':
			case 'u':
			case 'U':
				ans = 1;
				break;
			}
			break;

		case 'c':
		case 'C':
			switch (c2){
			case 'g':
			case 'G':
				ans = 1;
				break;
			}
			break;

		case 'a':
		case 'A':
			switch (c2){
			case 'u':
			case 'U':
				ans = 1;
				break;
			}
			break;

		case 'u':
		case 'U':
			switch (c2){
			case 'a':
			case 'A':
			case 'g':
			case 'G':
				ans = 1;
				break;
			}
			break;
		}
		return ans;
	}

	protected void findMaxBasePairs(String sequence){
		int len = sequence.length();
		double coterminus = 0, partitionable, sum;
		this.table = new double[len][len];
		initializeTable();

		for (int k = 1; k <= len-1; k++){
			for (int i = 0, j = i+k; i < len && j < len; i++, j++){
				try{
					coterminus = this.table[i+1][j-1] + getScore(sequence.charAt(i), sequence.charAt(j), i, j);
				} catch (ArrayIndexOutOfBoundsException e){
					System.out.println("ArrayIndexOutOfBoundsException");
					System.out.println(i);
					System.out.println(j);
					System.exit(-1);
				} catch (StringIndexOutOfBoundsException e){
					System.out.println("StringIndexOutOfBoundsException");
					System.out.println(i);
					System.out.println(j);
					System.exit(-1);
				} catch (Exception e){
					System.out.println(i);
					System.out.println(j);
					System.exit(-1);
				}

				partitionable = -1;
				for (int q = i; q < j; q++){
					sum = this.table[i][q] + this.table[q+1][j];
					if (sum > partitionable){
						partitionable = sum;
					}
				}
				this.table[i][j] = Math.max(coterminus, partitionable);
			}
		}
	}

	protected void fold(BufferedWriter bw){
		String sequence = "";
		try { 
			for (String name : this.sequences.keySet()){
				sequence = this.sequences.get(name);
				findMaxBasePairs(sequence);

				bw.write(">"+name);
				bw.newLine();
				bw.write(sequence);
				bw.newLine();
				System.out.println(">"+name);
				System.out.println(sequence);
				traceback(sequence);
				printTraceback(sequence, bw);

			} 
		} catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			for (StackTraceElement ste : e.getStackTrace()){
				System.out.println(ste.toString());
			}
		}
	}

	protected void run(){
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter("Output1.fasta"));
			fold(bw);
			bw.close();
		} catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}

	/*
	private void printTable(String sequence){
	//public void printTable(String sequence){
		int i, j, len = this.table.length;
		System.out.print("     ");
		for (i = 0; i < len; i++){
			System.out.format("%,3d",i);
			//			System.out.print(i+"  ");
		}
		System.out.println();
		System.out.print("       ");
		for (i = 0; i < len; i++){
			System.out.print(sequence.charAt(i)+"  ");
		}
		System.out.println();

		for (i = 0; i < len; i++){
			System.out.format("%,3d",i);
			System.out.print(" "+sequence.charAt(i));
			for (j = 0; j < len; j++){
				if (i-j >= 2){
					System.out.print("   ");
				}
				else{
					System.out.format("%,3d",(int)this.table[i][j]);  
					//					System.out.print(this.table[i][j]+" ");	
				}
			}
			System.out.println();
		}
		System.out.println();
	}
	*/

	//	private void traceback(String sequence){
	public void traceback(String sequence){
		int len = sequence.length();
		this.traceback = new char[len];
		Stack<int[]> stack = new Stack<int[]>();
		int [] arr = {0,len-1};
		int i, j;

		for (int l = 0; l < len; l++){
			this.traceback[l] = '.';
		}

		//		int numOfPairs = 0;

		stack.push(arr);

		while (!stack.empty()){
			arr = stack.pop();
			i = arr[0];
			j = arr[1];
			if (i >= j){
				continue;
			}else if (this.table[i+1][j] == this.table[i][j]){	// down
				arr = new int[2];
				arr[0] = i+1;
				arr[1] = j;
				stack.push(arr);
			}  else if (this.table[i][j-1] == this.table[i][j]){	// left
				arr = new int[2];
				arr[0] = i;
				arr[1] = j-1;
				stack.push(arr);
			} else if (this.table[i+1][j-1] + getScore(sequence.charAt(i), sequence.charAt(j), i, j) == this.table[i][j] && okPair(sequence.charAt(i), sequence.charAt(j))){ // diagonal
				this.traceback[i] = '(';
				this.traceback[j] = ')';
				//numOfPairs++;
				arr = new int[2];
				arr[0] = i+1;
				arr[1] = j-1;
				stack.push(arr);
			}  else {
				for (int k = i+1; k <= j-1; k++){	// partitionable
					if ( this.table[i][k] + this.table[k+1][j] == this.table[i][j]){
						arr = new int[2];
						arr[0] = k+1;
						arr[1] = j;
						stack.push(arr);
						arr = new int[2];
						arr[0] = i;
						arr[1] = k;
						stack.push(arr);
						break;
					}
				}
			}
		}
	}

	//	private void printTraceback(String sequence){
	public void printTraceback(String sequence, BufferedWriter bw){
		try { 
			String str = "";
			for (int i = 0; i < this.traceback.length; i++){
				str += this.traceback[i];
			}
			System.out.println(str);
			bw.write(str);
			bw.newLine();
		}  catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

	protected boolean okPair(char c1, char c2){
		boolean ret = false;
		switch (c1){
		case 'g':
		case 'G':
			switch (c2){
			case 'c':
			case 'C':
			case 'u':
			case 'U':
				ret = true;
				break;
			}
			break;

		case 'c':
		case 'C':
			switch (c2){
			case 'g':
			case 'G':
				ret = true;
				break;
			}
			break;

		case 'a':
		case 'A':
			switch (c2){
			case 'u':
			case 'U':
				ret = true;
				break;
			}
			break;

		case 'u':
		case 'U':
			switch (c2){
			case 'a':
			case 'A':
			case 'g':
			case 'G':
				ret = true;
				break;
			}
			break;
		}
		return ret;
	}
}
