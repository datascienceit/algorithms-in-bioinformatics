package Folder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Random;
import java.util.Stack;

public class NussinovImproved extends Nussinov {
	
	private double base;
	private int proximity;	// PROXIMITY = 1 cancels (), PROXIMITY = 2 cancels (()), PROXIMITY = 3 cancels ((())) and so forth
	
	public NussinovImproved(double base, int proximity){
		super();
		
		if (base != 0){
			this.base = base;
		} else{
			this.base = 8/9;
		}
		
		if (proximity != 0){
			this.proximity = proximity;
		} else{
			this.proximity = 2;
		}
	}

	protected void findMaxBasePairs(String sequence){
		int len = sequence.length();
		double coterminus = 0, partitionable, sum;
		this.table = new double[len][len];
		super.initializeTable();
		
		for (int k = 1; k <= len-1; k++){
			for (int i = 0, j = i+k; i < len && j < len; i++, j++){
				coterminus = 0;
				try{
					if ( (j-i+1)/2 > proximity)
						coterminus = this.table[i+1][j-1] + getScore(sequence.charAt(i), sequence.charAt(j), i, j);
				} catch (ArrayIndexOutOfBoundsException e){
					System.out.println("ArrayIndexOutOfBoundsException");
					System.out.println(i);
					System.out.println(j);
					System.exit(-1);
				} catch (StringIndexOutOfBoundsException e){
					System.out.println("StringIndexOutOfBoundsException");
					System.out.println(i);
					System.out.println(j);
					System.exit(-1);
				} catch (Exception e){
					System.out.println(i);
					System.out.println(j);
					System.exit(-1);
			}
				
				partitionable = -1;
				for (int q = i; q < j; q++){
					sum = this.table[i][q] + this.table[q+1][j];
					if (sum > partitionable){
						partitionable = sum;
					}
				}
				this.table[i][j] = Math.max(coterminus, partitionable);
			}
		}
	}
	
	/*
	private void printTraceback(){
		for (int i = 0; i <= this.traceback.length-1; i++){
			System.out.print((char)(this.traceback[i]));
		}
		System.out.println();
	}
	*/
	
	public void traceback(String sequence){
		int len = sequence.length();
		this.traceback = new char[len];
		Stack<int[]> stack = new Stack<int[]>();
		int [] arr = {0,len-1};
		int i, j;
		int [] pairs = new int[len];

		for (i = 0; i < len; i++){
			pairs[i] = -1;
		}

		for (int l = 0; l < len; l++){
			this.traceback[l] = '.';
		}

		int numOfStackedPairs = 0;

		stack.push(arr);
		while (!stack.empty()){
			//printTraceback();
			arr = stack.pop();
			i = arr[0];
			j = arr[1];
			if (i >= j){
				continue;
			} else if (this.table[i+1][j] == this.table[i][j]){
				numOfStackedPairs = 0;
				arr = new int[2];
				arr[0] = i+1;
				arr[1] = j;
				stack.push(arr);
			} else if (this.table[i][j-1] == this.table[i][j]){
				numOfStackedPairs = 0;
				arr = new int[2];
				arr[0] = i;
				arr[1] = j-1;
				stack.push(arr);
			} else if (this.table[i+1][j-1] + getScore(sequence.charAt(i), sequence.charAt(j), i, j) == this.table[i][j] && okPair(sequence.charAt(i), sequence.charAt(j))){
				if (numOfStackedPairs < 3){
					this.traceback[i] = '(';
					this.traceback[j] = ')';
					pairs[i] = j;
					pairs[j] = i;
					if (i+1 != j){
						numOfStackedPairs++;
					} else {
						numOfStackedPairs = 0;
					}
				} else {
					Random rand = new Random();
					if (rand.nextDouble() < Math.pow(base, numOfStackedPairs)){
						this.traceback[i] = '(';
						this.traceback[j] = ')';
						pairs[i] = j;
						pairs[j] = i;
						if (i+1 != j){
							numOfStackedPairs++;
						} else {
							numOfStackedPairs = 0;
						}
					}
				}
				
				arr = new int[2];
				arr[0] = i+1;
				arr[1] = j-1;
				stack.push(arr);
			}  else {
				numOfStackedPairs = 0;
				for (int k = i+1; k <= j-1; k++){
					if ( this.table[i][k] + this.table[k+1][j] == this.table[i][j]){
						arr = new int[2];
						arr[0] = k+1;
						arr[1] = j;
						stack.push(arr);
						arr = new int[2];
						arr[0] = i;
						arr[1] = k;
						stack.push(arr);
						break;
					}
				}
			}
		}

		//deleting all .(.  ...  .). pairs
		for (i = 1; i < len - 2; i++){
			if (pairs[i] != -1 && pairs[i-1] == -1 && pairs[i+1] == -1){
				j = pairs[i];
				if (j > 0 && j < len-1 && pairs[j] != -1 && pairs[j-1] == -1 && pairs[j+1] == -1){
					this.traceback[i] = '.';
					this.traceback[j] = '.';
				}
			}
		}
	}

	protected void run(){
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter("NussinovImproved.fasta"));
			fold(bw);
			bw.close();
		} catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
	}
}
