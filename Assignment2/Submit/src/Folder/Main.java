package Folder;

import java.io.IOException;

public class Main {
	
	static boolean base = false;
	static boolean proximity = false;
	
	public static void main(String[] args) throws IOException{
		String option = args[0];
		String inputFile = args[args.length-1];
		int proximityVal = 0;
		double baseVal = 0;
		
		switch (option){
			case "-n":
				Nussinov nussinovFolder = new Nussinov();
				nussinovFolder.parseFile(inputFile);
				nussinovFolder.run();
				break;
			case "-t":
				Thermodynamics thermoFolder = new Thermodynamics();
				thermoFolder.parseFile(inputFile);
				thermoFolder.run();
				break;
			case "-l":
				MutualInformation mIFolder = new MutualInformation();
				mIFolder.parseFile(inputFile);
				mIFolder.computeCovariationScores();
				mIFolder.run();
				break;
			case "-p":
				int i = 1;
				while (i < args.length-1){
					if (args[i].compareTo("-b") == 0){
						try {
							double val = Double.parseDouble(args[++i]);
							if (0 < val && val < 1){
								baseVal = val;
							} else{
								System.out.println("base parameter inserted is not a Double in the correct range. The correct range is (0, 1). The program will continue with default settings");
							}
							i++;
						} catch (Exception e) {
							System.out.println("base parameter inserted is not a Double in the correct range. The correct range is (0, 1). The program will continue with default settings");
						}
						if (baseVal > 0 && baseVal < 1){
							base = true;	
						}
					} else if (args[i].compareTo("-r") == 0){
						try{
							proximityVal = Integer.parseInt(args[++i]);
							i++;
						} catch (Exception e) {
							System.out.println("proximity parameter inserted is not an Integer, the program will continue with default settings");
						}
						if (proximityVal > 0){
							proximity = true;	
						}
					}
				}
				
				NussinovImproved nIFolder;
				
				if (base){
					if (proximity){
						nIFolder = new NussinovImproved(baseVal, proximityVal);
					} else {
						nIFolder = new NussinovImproved(baseVal, 0);
					}
				} else if (proximity){
					nIFolder = new NussinovImproved(0, proximityVal);
				} else {
					nIFolder = new NussinovImproved(0, 0);
				}
				
				nIFolder.parseFile(inputFile);
				nIFolder.run();
				break;
		}
	} // main
} // Class