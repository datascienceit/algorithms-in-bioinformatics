import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import com.sun.org.apache.xerces.internal.impl.dv.dtd.NMTOKENDatatypeValidator;


public class Tree {

	Vector<Vector<Node>> build;
	HashMap<String, Node> all_node;
	Node root;

	public Tree(Vector<Vector<List_score>> tree, BufferedWriter bw)
	{
		this.build = new Vector<Vector<Node>>();
		this.all_node = new HashMap<String, Node>();
		for(int i= 0; i<tree.size(); i++)
		{
			Vector<Node> temp = new Vector<Node>();
			this.build.add(temp);
		}
	}

	public void build_up(Vector<Vector<List_score>> hierarchial_Tree, int clusters,
			int labels,HashMap<String, Vector<String>> hash_labels,
			Vector<String> names_of_labels, Vector<String> rNAName, BufferedWriter bw)
	{
		String ans = "";
		for(int i=0; i<hierarchial_Tree.size(); i++)
		{
			for(int j=0; j<hierarchial_Tree.elementAt(i).size(); j++)
			{
				if(i==0)
				{
					Node temp = new Node(hierarchial_Tree,hierarchial_Tree.elementAt(i).elementAt(j).name,clusters, labels, hash_labels, names_of_labels,rNAName, i, all_node);
					this.build.get(i).add(temp);
					this.all_node.put(temp.name_Node, temp);
					//					temp.print();
				}
				else
				{
					if(hierarchial_Tree.elementAt(i).elementAt(j).name.contains("<>") && (this.all_node.get(hierarchial_Tree.elementAt(i).elementAt(j).name)==null))
					{
						ans = "("+ans+","+hierarchial_Tree.elementAt(i).elementAt(j).name+")";
						Node temp = new Node(hierarchial_Tree, this,hierarchial_Tree.elementAt(i).elementAt(j).name,clusters, labels, hash_labels, names_of_labels,rNAName, i, all_node);
						this.build.get(i).add(temp);
						this.all_node.put(temp.name_Node, temp);
						//						temp.print();
					}
				}
			}
		}
		this.root = this.build.lastElement().lastElement();
		this.root.type_node = "root";
		this.root.label = names_of_labels.elementAt(this.root.minIndexScore[clusters-1]);
	}


	public static void find_cluster(Node root, Vector<Node> root_Cluster,int sneeps, int missclasified, Vector<String> names_of_labels, BufferedWriter bw)
	{

		root_Cluster.add(root);
		find_cluster1(root, root_Cluster, sneeps, missclasified,  names_of_labels);
		for(int i=0; i<root_Cluster.size(); i++)
		{

			Vector<Node> temp = new Vector<Node>();
			find_new_cluster(root_Cluster.elementAt(i), temp);
			String type_cluster = find_type(temp,names_of_labels);
			print_cluster(temp, i, type_cluster, bw);
		}
	}


	private static void print_cluster(Vector<Node> temp, int i,String typeCluster, BufferedWriter bw)
	{
		try
		{
			System.out.println("cluster number : " +i);
			bw.write("<dt>> CLUSTER NUMBER: &nbsp;"+i+"</dt>");
			System.out.println("cluster type : " +typeCluster);
			bw.write("<dt>> CLUSTER TYPE: &nbsp;"+typeCluster+"</dt>");
			System.out.println();
			bw.write("<dl/>");
			bw.write("<br/>");
			for(int k=0; k<temp.size(); k++)
			{
				System.out.println("   label_type: "+temp.elementAt(k).label_type);
				System.out.println("   node_type: "+temp.elementAt(k).type_node);
				bw.write("<dd>node_name:&nbsp;"+temp.elementAt(k).name_Node+"<dd>");
				bw.write("<dd>label_type:&nbsp;"+temp.elementAt(k).label_type+"<dd>");
				System.out.println();
				bw.write("</dl>");
				bw.write("<br/>");
			}
			bw.write("</dl>");
			bw.write("<br/>");
			bw.write("</ol></dd>");
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static String find_type(Vector<Node> temp, Vector<String> namesOfLabels)
	{
		int temp_ans = -1;
		String ans = "";
		HashMap<String, Integer> hash = new HashMap<String, Integer>();
		for(int i=0; i<temp.size(); i++)
		{
			for(int j=0; j<temp.elementAt(i).label_type.size(); j++)
			{
				if(hash.get(temp.elementAt(i).label_type.elementAt(j))==null)
				{
					hash.put(temp.elementAt(i).label_type.elementAt(j), new Integer(1));
					if(temp_ans < hash.get(temp.elementAt(i).label_type.elementAt(j)))
					{
						temp_ans = hash.get(temp.elementAt(i).label_type.elementAt(j));
						ans = temp.elementAt(i).label_type.elementAt(j);
					}
				}
				else
				{
					hash.put(temp.elementAt(i).label_type.elementAt(j),hash.get(temp.elementAt(i).label_type.elementAt(j))+1);
					if(temp_ans < hash.get(temp.elementAt(i).label_type.elementAt(j)))
					{
						temp_ans = hash.get(temp.elementAt(i).label_type.elementAt(j));
						ans = temp.elementAt(i).label_type.elementAt(j);
					}
				}
			}
		}
		return ans;
	}

	private static void find_new_cluster(Node node, Vector<Node> temp)
	{
		if(node.type_node.equals("lift"))
		{
			temp.add(node);
		}
		else
		{
			if(node.right!=null)
			{
				find_new_cluster(node.right, temp);
			}
			if(node.left!=null)
			{
				find_new_cluster(node.left, temp);
			}
		}
	}

	public static void find_cluster1(Node root, Vector<Node> root_Cluster,int snips, int missclassified,  Vector<String> names_of_labels)
	{
		if(root.right !=null && root.left!=null && snips>0)
		{
			if(find_case4(root, root_Cluster, snips, missclassified, names_of_labels))
			{
			}
			else if(find_case3(root, root_Cluster, snips, missclassified, names_of_labels))
			{
			}
			else if(find_case2(root, root_Cluster, snips, missclassified, names_of_labels))
			{
			}
			else if(find_case1(root, root_Cluster, snips, missclassified, names_of_labels))
			{
			}
			else
			{
				System.out.println("big problem!!");
			}
		}
	}




	public static boolean find_case4(Node root,  Vector<Node> root_Cluster,int sneeps, int missclasified,  Vector<String> names_of_labels)
	{
		boolean ans = false;
		if(sneeps>1) // case 4
		{
			for(int i=0; i<=sneeps-2; i++) //TODO
			{
				if(root.minScore[sneeps] == (root.right.minScore[(sneeps-2)-i] + root.left.minScore[i]))
				{
					root.right.label = names_of_labels.elementAt(root.right.minIndexScore[sneeps-2-i]);
					root.left.label = names_of_labels.elementAt(root.left.minIndexScore[i]);
					root_Cluster.add(root.right);
					root_Cluster.add(root.left);
					find_cluster1(root.right, root_Cluster, (sneeps-2-i), root.right.minScore[(sneeps-2)-i], names_of_labels);
					find_cluster1(root.left, root_Cluster, i, root.left.minScore[i], names_of_labels);
					ans = true;
					root.right = null;
					root.left = null;
					break;
				}
			}
		}
		return ans;
	}

	public static boolean find_case3(Node root,  Vector<Node> root_Cluster,int sneeps, int missclasified,  Vector<String> names_of_labels)
	{
		boolean ans = false;
		for(int i=0; i<=sneeps-1; i++) // case 3
		{
			if(root.minScore[sneeps] == (root.right.minMiss[names_of_labels.indexOf(root.label)][(sneeps-1)-i] + root.left.minScore[i]))
			{
				root_Cluster.add(root.left);
				root.right.label = root.label;
				root.left.label = names_of_labels.elementAt(root.left.minIndexScore[i]);
				find_cluster1(root.right, root_Cluster, (sneeps-1-i), root.right.minScore[(sneeps-1)-i], names_of_labels);
				find_cluster1(root.left, root_Cluster, i, root.left.minScore[i], names_of_labels);
				root.left = null;
				ans = true;
				break;
			}
		}
		return ans;
	}


	public static boolean find_case2(Node root,  Vector<Node> root_Cluster,int sneeps, int missclasified,  Vector<String> names_of_labels)
	{
		boolean ans = false;
		for(int i=0; i<=sneeps-1; i++) //  case 2
		{
			if(root.minScore[sneeps] == (root.left.minMiss[names_of_labels.indexOf(root.label)][(sneeps-1)-i] + root.right.minScore[i]))
			{
				root_Cluster.add(root.right);
				root.left.label = root.label;
				root.right.label = names_of_labels.elementAt(root.right.minIndexScore[i]);
				find_cluster1(root.left, root_Cluster, (sneeps-1-i), root.left.minScore[(sneeps-1)-i], names_of_labels);
				find_cluster1(root.right, root_Cluster, i, root.right.minScore[i], names_of_labels);
				root.right = null;
				ans = true;
				break;
			}
		}
		return ans;
	}



	public static boolean find_case1(Node root,  Vector<Node> root_Cluster,int sneeps, int missclasified,  Vector<String> names_of_labels)
	{
		boolean ans = false;
		for(int i=0; i<=sneeps; i++) // case 1
		{
			if(root.minScore[sneeps] == (root.left.minMiss[names_of_labels.indexOf(root.label)][(sneeps)-i] + root.right.minMiss[names_of_labels.indexOf(root.label)][i]))
			{
				root.left.label = root.label;
				root.right.label = root.label;
				find_cluster1(root.left, root_Cluster, (sneeps-i), root.left.minScore[(sneeps)-i], names_of_labels);
				find_cluster1(root.right, root_Cluster, i, root.right.minScore[i], names_of_labels);
				ans = true;
				break;
			}
		}
		return ans;
	}

}
