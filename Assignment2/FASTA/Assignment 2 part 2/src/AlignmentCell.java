import java.util.Vector;

/**
 * implementation of a single cell for a regular alignment
 *
 * @author noam and motz
 *
 */
public class AlignmentCell {
	int score;
	int parent_I;
	int parent_j;

	/**
	 * empty constructor
	 */
	public AlignmentCell(){
		this.score=0;
		this.parent_I = 0;
		this.parent_j = 0;

	}


	/**
	 * contructor
	 * @param index_i i value of the cell
	 * @param index_j j value of the cell
	 * @param score current score
	 */
	public AlignmentCell(int index_i, int index_j, int score){
		this.score = score;
		this.parent_I = index_i;
		this.parent_j = index_j;
	}




}
