import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Vector;


/**
 * A collection of static alignment functions for PART 1
 * @author noam and motz
 *
 */
public class BuildMatrix {

	static final double A = 0.5;
	static final double B = 0.5;

/**
 * a global alignment implementation.
 * @param s1 first string
 * @param s2 second string
 * @param matrix scoring matrix
 * @param table the dynamic programming table for the algorithm.
 */
	public static double globalAlignment (String s1, String s2, ScoringMatrix matrix, AlignmentCell table[][])
	{
		table[0][0]=new AlignmentCell();
		for (int i=1; i<= s1.length();i++)
		{
			table[i][0]=new AlignmentCell((i-1),0,(table[i-1][0].score+matrix.score(s1.charAt(i-1)+"*")));

		}
		for (int j=1; j<= s2.length();j++)
		{
			table[0][j]=new AlignmentCell(0, (j-1), table[0][j-1].score+matrix.score("*"+s2.charAt(j-1)));

		}
		for (int i=1;i<=s1.length();i++)
		{
			for (int j=1; j<=s2.length();j++)
			{
				if((table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) >= (table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*")) &&
						(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) >=(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))))
				{
					table[i][j] = new AlignmentCell((i-1),(j-1),(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))));
				}
				// insert case
				else if((table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>=(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) &&
						(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>=(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))))
				{
					//					System.out.println("left case");
					table[i][j] = new AlignmentCell((i-1), (j), table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"));
				}
				// deleate case
				else
				{
					//					System.out.println("up case");
					table[i][j] = new AlignmentCell((i), (j-1), table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j)));
				}
			}
		}
		return table[s1.length()][s2.length()].score;
	}

/**
 * a local alignment implementation.
 * @param s1 first string
 * @param s2 second string
 * @param matrix scoring matrix
 * @param table the dynamic programming table for the algorithm.
 */
	public static double localAlignment (String s1, String s2, ScoringMatrix matrix, AlignmentCell table[][])
	{
		table[0][0]=new AlignmentCell();
		int max_I=0;
		int max_j=0;
		int max=0;
		for (int i=1; i<= s1.length();i++)
		{
			table[i][0]=new AlignmentCell(i-1,0,0);
		}
		for (int j=1; j<= s2.length();j++)
		{
			table[0][j]=new AlignmentCell(0,j-1,0);
		}
		for (int i=1;i<=s1.length();i++)
		{
			for (int j=1; j<=s2.length();j++)
			{


				if((table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) >= (table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*")) &&
						(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) >=(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))) &&
						table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))>0)
				{
					table[i][j] = new AlignmentCell(i-1, j-1,(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))));
					if(table[i][j].score>=max)
					{
						max=table[i][j].score;
						max_I=i;
						max_j=j;
					}
				}
				// insert
				else if((table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>=(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) &&
						(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>=(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))) &&
						(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>0)
				{
					table[i][j] = new AlignmentCell(i-1, j,(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*")));
					if(table[i][j].score>=max)
					{
						max=table[i][j].score;
						max_I=i;
						max_j=j;
					}
				}
				// deleate case
				else if((table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))) >= (table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) &&
						(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))) >=(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*")) &&
						(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j)))>=0)
				{
					table[i][j] = new AlignmentCell(i, j-1,(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))));
					if(table[i][j].score>=max)
					{
						max=table[i][j].score;
						max_I=i;
						max_j=j;
					}
				}
				else
				{
					table[i][j] = new AlignmentCell(0, 0, 0);
				}
			}
		}
		return table[max_I][max_j].score;
	}



/**
 * performs the gap alignment only for scoring purposes.
 * @param s1 the part of the chromosome which we align the EST against. according to the current window.
 * @param s2 the EST
 * @param indexEst number of EST from the list
 * @param matrix the scoring matrix
 * @param tableGap the table for the dynamic programming algorithm.
 * @param bw the writer for the html output.
 * @return the score for the current alignment
 */
	public static double Gap_PinT_Alignment1(PartialChromosome s1, String s2,int indexEst, ScoringMatrix matrix, GapCell[][] tableGap,BufferedWriter bw)
	{
		tableGap[0][0]=new GapCell(0,0,0,0,0);
		for (int i=1; i<= s1.Partial.length();i++)
		{
			tableGap[i][0]=new GapCell(0,0,0,i-1,0);
		}
		for (int j=1; j<= s2.length();j++)
		{
			tableGap[0][j] = new GapCell(-j,-j,-j,0,j-1);
		}
		for (int i=1;i<=s1.Partial.length();i++)
		{
			for (int j=1; j<=s2.length();j++)
			{
				tableGap[i][j] = new GapCell(0, 0, 0, 0, 0);
				tableGap[i][j].SetGapCell(
						tableGap[i-1][j-1].V+matrix.score(s1.Partial.substring(i-1,i)+s2.substring(j-1,j)),
						tableGap[i][j-1].V+matrix.score("*"+s2.substring(j-1,j)),
						Math.max(Math.max(tableGap[i-1][j].G, tableGap[i-1][j].E)+w(1), tableGap[i-1][j].F-B),
						tableGap, A, B, i, j);
			}
		}
		double maxScore = tableGap[0][s2.length()].V;
		for(int i = 1; i<= s1.Partial.length();  i++)
		{
			if(maxScore < tableGap[i][s2.length()].V)
			{
				maxScore=tableGap[i][s2.length()].V;
			}
		}
		return maxScore;
	}




/**
 * the affine gap scoring function
 * @param i size of gap
 * @return gap penalty for a gap of size i
 */
	private static double w (int i)
	{
		return -A-B*i;
	}



	public static void BuildMatrix(Vector<String> RNAsequences, Vector<String> RNAName, ScoringMatrix matrix,Vector<List_score> distance_matrix, BufferedWriter bw)
	{
		int run=0;
		for(int i=0; i<RNAsequences.size(); i++)
		{
			List_score temp = new List_score(RNAName.elementAt(i),i, Integer.MIN_VALUE, -1);
			for(int k=0; k<run; k++)
			{
				temp.scores.add(distance_matrix.elementAt(k).scores.elementAt(i));
				if(temp.max<=temp.scores.lastElement())
				{
					temp.max = temp.scores.lastElement();
					temp.index_max = temp.scores.size()-1;
				}
			}
			for(int j=run; j<RNAsequences.size(); j++)
			{
				if(i==j)
				{
					temp.scores.add((double) Integer.MIN_VALUE);
					temp.index_zero = j;
				}
				else
				{
					AlignmentCell [][] alignment = new AlignmentCell[RNAsequences.elementAt(i).length()+1][RNAsequences.elementAt(j).length()+1];
					temp.scores.add(globalAlignment(RNAsequences.elementAt(i), RNAsequences.elementAt(j), matrix, alignment));
					if(temp.max<=temp.scores.lastElement())
					{
						temp.max = temp.scores.lastElement();
						temp.index_max = temp.scores.size()-1;
					}
				}
			}
			run++;
			distance_matrix.add(temp);
//			System.out.println("finish list number: "+ i);
		}
	}

	public static void printMatrix(Vector<List_score> distance_matrix)
	{
		for(int i=0; i<distance_matrix.size(); i++)
		{
			for(int j=0; j<distance_matrix.elementAt(i).scores.size(); j++)
			{
				System.out.print(distance_matrix.elementAt(i).scores.elementAt(j)+"     ");
			}
			System.out.println();
		}
	}

}
