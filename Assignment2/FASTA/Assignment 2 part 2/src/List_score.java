import java.util.Vector;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;


public class List_score implements Comparable<List_score> {

	public String name;
	public int index_name;
	public Vector<Double> scores;
	public double max;
	public int index_max;
	public int index_zero;



	public List_score(String name, int index_name, double max, int index_max)
	{
		this.name =name;
		this.index_name = index_name;
		this.max = max;
		this.index_max =index_max;
		this.scores = new Vector<Double>();
	}

	public List_score(String name, int index_name, double max, int index_max, Vector<Double> temp)
	{
		this.name =name;
		this.index_name = index_name;
		this.max = max;
		this.index_max =index_max;
		this.scores = new Vector<Double>();
		for(int i=0; i<temp.size(); i++)
		{
			this.scores.add(temp.elementAt(i));
		}
		this.scores = temp;
	}


	public void set_index_max()
	{
		this.index_max = -1;
		this.max = Integer.MIN_VALUE;
		for(int i=0; i<this.scores.size(); i++)
		{
			if(this.scores.elementAt(i)>this.max)
			{
				this.index_max = i;
				this.max = this.scores.elementAt(i);
			}
		}
	}

	public void set_index_zero()
	{
		for(int i=0; i<this.scores.size(); i++)
		{
			if(this.scores.elementAt(i)==0)
			{
				this.index_zero = i;
				break;
			}
		}
	}

	@Override
	public int compareTo(List_score o) {
		return (int) (o.max-this.max);
	}
}

