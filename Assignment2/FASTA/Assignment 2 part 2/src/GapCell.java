/**
 * implementation of a cell in the dynamic programming table that is used on the variant alignment.
 * this one uses more fields that a regular cell because of the implementation of the affine
 * gap function which helps to reduce running time.
 * @author noam and motz
 *
 */
public class GapCell {

	double  G;
	double E;
	double V;
	double F;
	int parent_I;
	int parent_j;
/**
 * constructor G E and F are field as described by the algorithm for alignment with affine gap function.
 * @param G match\mismatch value
 * @param E insert value
 * @param F delete value
 * @param parentI
 * @param parentJ
 */
	public GapCell(double G, double E, double F, int parentI, int parentJ)
	{
		this.G = G;
		this.E = E;
		this.F = F;
		this.V = Math.max(Math.max(this.G, this.E), this.F);
		this.parent_I = parentI;
		this.parent_j = parentJ;
	}

	/**
	 * a function that picks the best score for a current cell considering cells which have been
	 * already calculated. also determines it's parent for the alignment restoration.
	 * @param G
	 * @param E
	 * @param F
	 * @param tableGap
	 * @param A A value for the affine gap function (A+B*x)
	 * @param B B value for the affine gap function (A+B*x)
	 * @param i i coordinate of current cell
	 * @param j j coordinate of current cell
	 */
	public void SetGapCell(double G, double E, double F, GapCell[][] tableGap, double A, double B, int i, int j)
	{
		this.G = G;
		this.E = E;
		this.F = F;
		this.V = Math.max(Math.max(this.G, this.E), this.F);
		if(this.V==this.G)
		{
			this.parent_I = i-1;
			this.parent_j = j-1;
		}
		else if(this.V==this.E)
		{
			this.parent_I = i;
			this.parent_j = j-1;
		}
		else
		{
			if(this.V==tableGap[i-1][j].G-(B+A))
			{
				this.parent_I = i-1;
				this.parent_j = j;
			}
			else if(this.V==tableGap[i-1][j].E-(B+A))
			{
				this.parent_I = i-1;
				this.parent_j = j;
			}
			else //if(this.V==tableGap[i-1][j].F-B)
			{
				this.parent_I = tableGap[i-1][j].parent_I;
				this.parent_j = tableGap[i-1][j].parent_j;
			}

		}
	}

}
