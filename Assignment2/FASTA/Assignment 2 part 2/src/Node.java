import java.util.HashMap;
import java.util.Vector;


public class Node {

	Node left;
	Node right;
	String name_Node;
	Vector<String> label_type;
	int [][] minMiss;
	int [] minScore;
	int minIndexScore[];
	int lavel;
	String label;
	String type_node;
	boolean in_cluster;


	public Node(Vector<Vector<List_score>> hierarchial_Tree, String name, int clusters,
			int labels, HashMap<String, Vector<String>> hash_labels,
			Vector<String> names_of_labels, Vector<String> rNAName, int index_label, HashMap<String, Node> all_node)
	{
		this.name_Node = name;
		this.in_cluster = true;
		this.type_node = "lift";
		this.lavel = index_label;
		this.label = "";
		this.minIndexScore = new int [clusters];
		this.label_type = new Vector<String>();
		for(int i=0; i<names_of_labels.size(); i++)
		{
			if(hash_labels.get(names_of_labels.elementAt(i)).contains(this.name_Node))
			{
				this.label_type.add(names_of_labels.elementAt(i));
			}
		}
		this.left = null;
		this.right = null;
		this.minMiss = bild_minMiss(left, right, labels, clusters,names_of_labels);
		this.minScore = bild_minScore(this.minMiss, clusters, labels, this.minIndexScore);
	}


	public Node(Vector<Vector<List_score>> hierarchial_Tree,Tree tree ,String name, int clusters,
			int labels, HashMap<String, Vector<String>> hash_labels,
			Vector<String> names_of_labels, Vector<String> rNAName, int index_label,HashMap<String, Node> all_node)
	{
		this.name_Node = name;
		this.in_cluster = true;
		this.type_node = "internal";
		this.lavel = index_label;
		this.label = "";
		this.minIndexScore = new int [clusters];
		this.label_type = new Vector<String>();
		for(int i=0; i<hierarchial_Tree.elementAt(index_label-1).size(); i++)
		{
			for(int j=i; j<hierarchial_Tree.elementAt(index_label-1).size(); j++)
			{
				if((hierarchial_Tree.elementAt(index_label-1).elementAt(i).name+"<>"+hierarchial_Tree.elementAt(index_label-1).elementAt(j).name).equals(this.name_Node) ||
						(hierarchial_Tree.elementAt(index_label-1).elementAt(j).name+"<>"+hierarchial_Tree.elementAt(index_label-1).elementAt(i).name).equals(this.name_Node))
				{
					this.left = all_node.get(hierarchial_Tree.elementAt(index_label-1).elementAt(i).name);
					this.right = all_node.get(hierarchial_Tree.elementAt(index_label-1).elementAt(j).name);
					break;
				}
			}
			this.minMiss = bild_minMiss(this.left, this.right, labels, clusters, names_of_labels);
			this.minScore = bild_minScore(this.minMiss,clusters, labels, this.minIndexScore);
		}
	}


	private int[][] bild_minMiss(Node left, Node right, int labels,
			int clusters, Vector<String> names_of_labels)
	{
		int [][]temp = new int[labels][clusters];
		for(int i=0; i<labels; i++)
		{
			for(int j=0; j<clusters; j++)
			{
				if(this.left == null && this.right == null) //case label 0
				{
					if(this.label_type.contains(names_of_labels.elementAt(i)) || j > 0)
					{
						temp[i][j]=0;
					}
					else
					{
						temp[i][j]=1;
					}
				}
				else
				{
					int case1 = find_case1(this.right, this.left, i,j);
					int case2 = find_case2(this.right, this.left, i,j);
					int case3 = find_case3(this.right, this.left, i,j);
					int case4 = find_case4(this.right, this.left, i,j);
//					System.out.println("  case1: " + case1+"  case2: " + case2+"  case3: " + case3+"  case4: " + case4);
					temp[i][j] = find_min(case1, case2, case3,case4);
				}
			}
		}
		return temp;
	}


	private int find_case1(Node right2, Node left2, int l, int k)
	{
		int ans = Integer.MAX_VALUE;
		int temp = -1;
		for(int i=0; i<=k; i++)
		{
			temp = (left2.minMiss[l][i] + right2.minMiss[l][k-i]);
			if(ans > temp)
			{
				ans = temp;
			}
		}
//		System.out.println("temp - case1: " + ans);
		return ans;
	}


	private int find_case2(Node right2, Node left2, int l, int k)
	{
		int ans = Integer.MAX_VALUE;
		int temp = -1;
		for(int i =0; i<k; i++)
		{
			temp = (left2.minMiss[l][i] + right2.minScore[k-i-1]);
			if(ans > temp)
			{
				ans = temp;
			}
		}
//		System.out.println("temp - case2: " + ans);
		return ans;
	}


	private int find_case3(Node right2, Node left2, int l, int k)
	{
		int ans = Integer.MAX_VALUE;
		int temp = -1;
		for(int i =0; i<k; i++)
		{
			temp = (left2.minScore[i] + right2.minMiss[l][k-i-1]);
//			System.out.println("temp - case3: " + temp);
			if(ans > temp)
			{
				ans = temp;
			}
		}
//		System.out.println("temp - case3: " + ans);
		return ans;
	}


	private int find_case4(Node right2, Node left2, int l, int k)
	{
		int ans = Integer.MAX_VALUE;
		int temp = -1;
		for(int i =0; i<k-1; i++)
		{
			temp = (left2.minScore[i] + right2.minScore[k-i-2]);
//			System.out.println("before - case4: " + temp);
			if(ans > temp)
			{
				ans = temp;
			}
		}
//		System.out.println("temp - case4: " + ans);
		return ans;
	}


	private int find_min(int case1, int case2, int case3, int case4) {
		int ans1,ans2;
		ans1 = Math.min(case1, case2);
		ans2 = Math.min(case3, case4);
		return  Math.min(ans1, ans2);
	}


	private int[] bild_minScore(int[][] minMiss, int clusters,int labels, int [] minIndexScore)
	{
		int[] temp = new int[clusters];
		for(int i=0; i<clusters; i++)
		{
			temp[i] = minMiss[0][i];
			minIndexScore[0]= minMiss[0][i];
			for(int j=1; j<labels; j++)
			{
				if(minMiss[j][i] <= temp[i])
				{
					temp[i]=minMiss[j][i];
					minIndexScore[i] = j;
				}
			}
		}
		return temp;
	}



	public void print()
	{
		System.out.println();
		System.out.println();
		System.out.println("informotion for new node!!!!");
		System.out.println("NODE - name: "+this.name_Node);
		System.out.println("NODE - label_type: "+this.label_type);
		System.out.println("NODE - type: "+this.type_node);
		if(this.right!=null)
			System.out.println("right :"+this.right.name_Node);
		if(this.left!=null)
			System.out.println("left :"+this.left.name_Node);
		System.out.println("label: "+this.lavel);
		System.out.println("minMiss:");
		for(int i=0; i<this.minMiss.length; i++)
		{
			for(int j=0; j<this.minMiss[i].length; j++)
			{
				System.out.print(this.minMiss[i][j]+" ,");
			}
			System.out.println();
		}
		System.out.println("minScore:");
		for(int j=0; j<this.minScore.length; j++)
		{
			System.out.print(this.minScore[j]+" ,");
		}

		System.out.println();
	}


	public String getName() {
		if (this.right==null && this.left==null)
			return this.name_Node;
		else
			return "("+this.left.getName()+","+this.right.getName()+")";
	}
}