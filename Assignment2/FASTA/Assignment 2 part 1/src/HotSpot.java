import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

/**
 * implementation of a hotspot for the fasta algorithm.
 * @author hazann
 *
 */
public class HotSpot implements Comparable<HotSpot> {

	int Index_I_begin;
	int Index_I_End;
	int Index_J_begin;
	int Index_J_End;
	int scoreHotSpot;


	/**
	 * empty contructor
	 */
	public HotSpot()
	{
		Index_I_begin = 0;
		Index_I_End = 0;
		Index_J_begin = 0;
		Index_J_End = 0;
		this.scoreHotSpot = 0;

	}

/**
 * constructor
 * @param Ibegin
 * @param Iend
 * @param Jbegin
 * @param Jend
 * @param score
 */
	public HotSpot(int Ibegin, int Iend, int Jbegin, int Jend, int score)
	{
		this.Index_I_begin = Ibegin;
		this.Index_I_End = Iend;
		this.Index_J_begin =Jbegin;
		this.Index_J_End = Jend;
		this.scoreHotSpot +=score;
	}



/**
 * checks if a given hotspot overlaps with current one
 * @param temp given hotspot
 * @param distanse distance allowed between hotspots
 * @param RADIUS how far apart 2 diagonals that contain hotspots are allowed.
 * @return
 */
	public boolean overLap(HotSpot temp, int distanse, int RADIUS)
	{
//		System.out.println("function: overLap in HotSpot");
		boolean ans = true;
		if(((this.Index_I_begin < temp.Index_I_End) && (this.Index_I_begin>temp.Index_I_begin))
				|| (Math.abs(this.Index_I_begin-this.Index_J_begin))-(distanse)>RADIUS)
		{
			ans = false;
			return ans;
		}
		return ans;
	}


/**
 * comparing by score
 */
	public int compareTo(HotSpot o) {
		return this.scoreHotSpot-o.scoreHotSpot;
	}


}
