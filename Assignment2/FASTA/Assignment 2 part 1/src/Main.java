import java.awt.image.CropImageFilter;
import java.io.*;
import java.util.Vector;



class main
{
	public static void main(String args[]) // arg0 : est list, arg1: chr7, arg2: scoring matrix
	{
		boolean print = true;
		int overLap = 5000; //TODO
		int  SIZE = 200;
		Vector<String> ests = new Vector<String>();//TODO
		Vector<PartialChromosome> chromozome = new Vector<PartialChromosome>();
		ScoringMatrix matrix = new ScoringMatrix(args[2]);//TODO
		String chromosome = new String();
		//-------------------->
		String est = new String();
		AlignmentCell[][] table;
		GapCell[][] tableGap;
		int line = 0;
		int begin = 1;
		int end = 10000;
		//-------------------->
		try
		{
			FileInputStream fasta = new FileInputStream(args[1]);//TODO args[1]
			DataInputStream in = new DataInputStream(fasta);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			String temp = "";
			while ((strLine = br.readLine()) != null) // TODO read the ests put into vector
			{

				if ((strLine.startsWith(">")))
				{
					ests.add(temp);
					temp = "";
				}
				else
				{
					temp = temp +strLine;
				}

			}
			ests.remove(0);
			ests.add(temp);
		}
		catch (Exception e) {
			System.err.println("Error openning ests " + e.getMessage());
		}

		// ################################## CASE WITHOUT FILTER ########################################
		try
		{
			FileInputStream fasta = new FileInputStream(args[0]);//TODO args[2]
			DataInputStream in = new DataInputStream(fasta);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine = br.readLine();

			while ((strLine = br.readLine()) != null) // read the ests put into vector
			{
				chromosome = chromosome+strLine;
				line++;
				if((line % SIZE) == 0)
				{
//										System.out.println();
//										System.out.println("line: "+line);
					PartialChromosome temp = new PartialChromosome();
					temp.Partial = chromosome;
				//						System.out.println("temp length: "+temp.Partial.length());
					temp.indexBegin = begin;
				//						System.out.println("begin: "+temp.indexBegin);
					temp.indexEnd = end;
				//						System.out.println("end: "+temp.indexEnd);
					chromozome.add(temp);
					chromosome = chromosome.substring(5000);
					begin = end-overLap+1;
					end = 5000+end;
					line = line+100;
				}

			}
			if((line % SIZE)!=0)
			{
				PartialChromosome temp = new PartialChromosome();
				temp.Partial = chromosome;
			//	System.out.println("temp length: "+temp.Partial.length());
				temp.indexBegin = begin;
			//	System.out.println("begin: "+temp.indexBegin);
				temp.indexEnd = begin+chromosome.length();
			//	System.out.println("end: "+temp.indexEnd);
				chromozome.add(temp);
			}
		}
		catch (Exception e) {
			System.err.println("Error openning chromosome " + e.getMessage());
		}
//############################################ END CASE WITHOUT FILTER ########################################################

/*

// ################################## CASE FASTA ########################################
		try
		{
			FileInputStream fasta = new FileInputStream(args[0]);//TODO args[2]
			DataInputStream in = new DataInputStream(fasta);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null) // read the ests put into vector
			{
				chromosome = chromosome+strLine;
				line++;
				if((line % SIZE) == 0)
				{
					//					System.out.println();
					//					System.out.println("line: "+line);
					PartialChromosome temp = new PartialChromosome();
					temp.Partial = chromosome;
					//					System.out.println("temp length: "+temp.Partial.length());
					temp.indexBegin = begin;
					//					System.out.println("begin: "+temp.indexBegin);
					temp.indexEnd = end;
					//					System.out.println("end: "+temp.indexEnd);
					chromozome.add(temp);
					chromosome = chromosome.substring(9000);
					begin = end-overLap+1;
					end = 9000+end;
					line = line+20;
				}

			}
			if((line % SIZE)!=0)
			{
				PartialChromosome temp = new PartialChromosome();
				temp.Partial = chromosome;
				temp.indexBegin = begin;
				temp.indexEnd = begin+chromosome.length();
				chromozome.add(temp);
			}
		}
		catch (Exception e) {
			System.err.println("Error openning chromosome " + e.getMessage());
		}
//############################################ END CASE FASTA ########################################################
*/
		long startTime=System.currentTimeMillis();


		//		PartialChromosome temp= new PartialChromosome();
		//		temp.Partial = chromosome;
		//		table = new AlignmentCell[chromosome.length()+1][est.length()+1];
		//		tableGap = new GapCell[chromosome.length()+1][est.length()+1];
		//				System.out.println("chromosome T:   "+chromosome);
		//				System.out.println("est P:          "+est);
		//		Alignments.globalAlignment(est,chromosome, matrix,table);
		//		Alignments.Gap_PinT_Alignment(temp, est, 1, matrix, tableGap);
		//		System.out.println();
		//		Alignments.localAlignment(est,chromosome, matrix,table);
		//		Fasta fastaFilter = new Fasta(chromosome, est);
		//		fastaFilter.checkFasta(ests, chromosome, est, matrix, table);
		//		Filter temp =new Filter(ests.elementAt(0), ests.elementAt(0));
		//		if(temp.countDiagonal>Filter.LOWERBOUNDS)
		//		{
		//			Alignments.globalAlignment(ests.elementAt(0),ests.elementAt(0), matrix,table);
		//			System.out.println(temp.countDiagonal);
		//		}
		//		System.out.println(temp.countDiagonal);



		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter("Q2_output.html"));
			bw.write("<html><font size=\"5\" face='Lucida Console'><body><center><h2>Aligning ESTs to cromosome 7</h2></center><dl>");

			// ####################### CASE FASTA ######################################

			Filter.AlignmentAfterFilter(chromozome, ests, matrix,bw);

			// ####################### CASE FASTA ######################################

//############################################ CASE WITHOUT FILTER ########################################################
/*
			int chrMax = -1;
			double temp = -1;
			double temp1 = -1;
			for(int i=0; i<ests.size(); i++)
			{
				for(int j=0; j<chromozome.size(); j++)
				{
					tableGap = new GapCell[chromozome.elementAt(j).Partial.length()+1][ests.elementAt(i).length()+1];
					temp = Alignments.Gap_PinT_Alignment1(chromozome.elementAt(j), ests.elementAt(i), chrMax, matrix, tableGap, bw);
				//	System.out.println("finish chr "+ j+" score: "+ temp);

					if(temp>temp1)
					{
						temp1=temp;
						chrMax = j;
					}
				}
				tableGap = new GapCell[chromozome.elementAt(chrMax).Partial.length()+1][ests.elementAt(i).length()+1];
				Alignments.Gap_PinT_Alignment(chromozome.elementAt(chrMax), ests.elementAt(i), i, matrix, tableGap, bw);
				chrMax = -1;
				temp = -1;
				temp1 = -1;
				System.out.println("finish est "+ i+2);
			}
*/
//############################################ END CASE WITHOUT FILTER ########################################################

			bw.write("</body></html>");
			bw.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Overall time: "+ (System.currentTimeMillis()-startTime)/1000);
	}
	
	//##################################################################################################################//

}