import java.util.ArrayList;
import java.util.Stack;


public class NussinovJacobson {

	public static final int NUM_OF_SOLUTIONS=5;
	/**
	 * implementation of nussinov algorithm
	 * @param rna the sequence we wish to predict it's secondary structure
	 */
	public static void fold(String rna)
	{
		//initialization
		int D[][]=new int[rna.length()][rna.length()];
		//		for( int k=0; k<=1; k++)
		//			for( int i=0; i<=rna.length()-1-k;i++)
		//				D[i][i+k]=0;

		//Recurrence
		for(int j=0;j<rna.length();j++)
		{
			for (int i=rna.length()-1;i>=0;i--)
			{
				D[i][j]=0;
				if (j>i) //added so there would be a minimum of 3 nucleotides per loop 
				{
					for (int k=i+1;k<j;k++) // check all partitions
					{
						D[i][j]=Math.max(D[i][j], D[i][k]+D[k+1][j]);
					}
					D[i][j]=Math.max(D[i][j], D[i+1][j-1]+score(rna.charAt(i),rna.charAt(j)));
				}
			}
		}

		//restoring NUM_OF_SOLUTIONS optimal solutions
		char solutions[][]=new char[NUM_OF_SOLUTIONS][rna.length()];
		ArrayList<Stack<Pair>> stacks= new ArrayList<Stack<Pair>>();
		for (int i=0; i<NUM_OF_SOLUTIONS;i++)
			stacks.add(new Stack<Pair>());

		for (int i=0; i< rna.length();i++) // print the table (debugging)
		{
			for (int j=0; j<rna.length();j++)
			{
				System.out.print(D[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("Best Score: "+D[0][rna.length()-1]);
		Stack<Pair> stack =stacks.get(0);
		stack.push(new Pair(0,rna.length()-1));
		for (int i=0; i<NUM_OF_SOLUTIONS; i++)
		{
			restoreSolutions(solutions,stacks,i,rna,D);
		}
	}

	/**
	 * restoring the solution after finishing the dynamic programming algorithm by Nussinov-Jacobson
	 * @param solutions 5 arrays of solutions in dot-bracket format
	 * @param stacks stacks for the recursive solution restore algorithm
	 * @param sol solution number
	 * @param rna rna sequence
	 * @param D the table from nussinov algorithm
	 */
	private static void restoreSolutions(char[][] solutions,ArrayList<Stack<Pair>> stacks, int sol, String rna,int[][] D) { 
		Stack<Pair> stack=stacks.get(sol);
		Stack<Pair> tempStack=new Stack<Pair>();
		while (!stack.isEmpty())
		{
			int found=0;
			int recorded=0;
			Pair p=stack.pop();
			if (p.i>=p.j)
				continue;
			if (D[p.i+1][p.j-1]+score(rna.charAt(p.i),rna.charAt(p.j))==D[p.i][p.j])
			{
				if (score(rna.charAt(p.i),rna.charAt(p.j))>0)
				{
					solutions[sol][p.i]='(';
					solutions[sol][p.j]=')';
					recorded=1;
					
				}
				tempStack.push(new Pair(p.i+1,p.j-1));
				found=1;
			}
			//else
			for ( int k=p.i+1; k<p.j; k++)
			{
				if (D[p.i][k]+D[k+1][p.j]==D[p.i][p.j] && D[p.i][p.j]!=0)
				{
					if (found>0)
					{
						if (recorded>0)
						{
							for(int i=sol+1;i<NUM_OF_SOLUTIONS;i++)
							{
								if (stacks.get(i).isEmpty())
								{
									stacks.set(i,stackDeepCopy(stack));
									stacks.get(i).push(new Pair(k+1,p.j));
									stacks.get(i).push(new Pair(p.i,k));
									copySolution(solutions, p, sol, i);
									break;
								}
							}
						}
					}
					else
					{
						tempStack.push(new Pair(k+1,p.j));
						tempStack.push(new Pair(p.i,k));
						found=1;
					}
				}
				//break;
			}
			while (!tempStack.isEmpty())
				stack.push(tempStack.pop());
		}
		

		for (int k=0; k<rna.length();k++) // print the bracket dot folding
		{
			if (solutions[sol][k]==0)
				System.out.print(".");
			else
				System.out.print(solutions[sol][k]);
		}
		System.out.println();

	}





	/**
	 * copying the solution so far not including the current point. (since it has been recorded)
	 * @param solutions
	 * @param p
	 * @param from
	 * @param to
	 */
	private static void copySolution(char[][] solutions, Pair p, int from, int to) {
		for (int i=0;i<solutions[from].length; i++)
			solutions[to][i]=solutions[from][i];
		solutions[to][p.i]=0;
		solutions[to][p.j]=0;

	}

	/**
	 * deep copy a stack
	 * @param input
	 * @return
	 */
	private static Stack<Pair> stackDeepCopy(Stack<Pair> input)
	{
		Stack<Pair> temp=new Stack<Pair>();
		Stack<Pair> ans=new Stack<Pair>();
		while (!input.isEmpty())
			temp.push(input.pop());
		while (!temp.isEmpty())
			ans.push(temp.pop());
		return ans;
	}

	/**
	 * calculates score of 2 characters 1 if they can be connected 0 otherwise
	 * @param c1
	 * @param c2
	 * @return
	 */
	private static int score(char c1, char c2) {
		if (c1=='A')
		{
			if (c2=='U' | c2=='T')
				return 1;
			else
				return 0;
		}
		if (c1=='U')
		{
			if (c2=='A')
				return 1;
			else
				return 0;
		}
		if (c1=='C')
		{
			if (c2=='G')
				return 1;
			else
				return 0;
		}
		if (c1=='G')
		{
			if (c2=='C' | c2=='T')
				return 1;
			else
				return 0;
		}
		if (c1=='T')
		{
			if (c2=='G' | c2=='A')
				return 1;
			else
				return 0;
		}
		return 0;
	}
}


