import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

/**
 * an objet representing a table diagonal for the fasta filtration.
 * @author noam and motz
 *
 */
public class Diagonal implements Comparable<Diagonal>{

	Vector<HotSpot> diagonl;
	int scoreDiagonal;
	int index_I_begin;
	int index_J_begin;
	int index_I_end;
	int index_J_end;

	static final int  LENGTH = 2;
	static final int FINE_MERGE_HOT_SPOT = 1;
	static final int DISTANCE_HOT_SPOT = 1; // add 1
	static final int NUM_OF_BEST_SCORING_REGION = 3;
	static final int RADIUS = 3;

/**
 * an empty constructor
 */
	public Diagonal()
	{
		this.diagonl = new Vector<HotSpot>();
		this.scoreDiagonal = Integer.MIN_VALUE;
		this.index_I_begin = 0;
		this.index_J_begin = 0;
		this.index_I_end = 0;
		this.index_J_end = 0;
	}


/**
 * a copy constructor
 * @param diagonal old diagonal
 * @param score score of old diagonal
 */
	public Diagonal(Vector<HotSpot> diagonal, int score)
	{
		this.diagonl = diagonl;
		this.scoreDiagonal = score;
	}

	/**
	 * finding the best diagonal available on which we would want to perform an alignment.
	 * @param hash a hash map of all the diagonals
	 * @param T big string (chromosome)
	 * @param P small string (EST)
	 * @param length minimal length allowed for a diagonal. the thresholod.
	 * @return the best diagonal
	 */
	public static  Diagonal findDiagonal(HashMap<String, Vector<Integer>> hash, String T, String P, int length) {
		System.out.println("function: findDiagonal in DIagonl");
		Diagonal ans = new Diagonal();
		HashMap<Integer, Vector<HotSpot>> hotSpotDB = initHotSpot(hash, LENGTH, T, P);
		combineHotSpot(hotSpotDB, T, P, LENGTH);
		ans.diagonl = returMaxHotSpot(hotSpotDB, T, P, LENGTH);
		ans.findBestCombination();

//		#########################################################
//				System.out.println("ans.index_I_begin "+ans.index_I_begin);
//				System.out.println("ans.index_J_begin "+ans.index_J_begin);
//				System.out.println("ans.index_I_end "+ans.index_I_end);
//				System.out.println("ans.index_J_end "+ans.index_J_end);
//				System.out.println("ans.scoreDiagonal "+ans.scoreDiagonal);
//		#########################################################

		return ans;
	}


	/**
	 * created the hashmap of all the hotspots.
	 * @param hash the hash map of the hot spots
	 * @param length minimal length of hotspot
	 * @param T big string (chromosome)
	 * @param P small string (EST)
	 * @return the hot spot hash map
	 */
	public static HashMap<Integer, Vector<HotSpot>> initHotSpot(HashMap<String, Vector<Integer>>  hash, int length, String T, String P)
	{
		System.out.println("function: initHotSpot in DIagonl");
		HashMap<Integer, Vector<HotSpot>> hotSpotDB = new HashMap<Integer, Vector<HotSpot>>();
		for(int i=length-T.length(); i<=(P.length()-length); i++)
		{
			Vector<HotSpot> tempVector = new Vector<HotSpot>();
			hotSpotDB.put(i, tempVector);
		}
		for(int i=0; i<=P.length()-length; i++)
		{
			String temp = P.substring(i, i+length);
			if(hash.get(temp)!=null)
			{
				for(int j=0; j<hash.get(temp).size(); j++)
				{
					HotSpot tempHotSpot = new HotSpot(i,hash.get(temp).elementAt(j) , (i+length-1), (hash.get(temp).elementAt(j)+length-1), length);
					if(hotSpotDB.get((i-hash.get(temp).elementAt(j)))!= null)
					{
						hotSpotDB.get((i-hash.get(temp).elementAt(j))).add(tempHotSpot);
					}
					else
					{
						Vector<HotSpot> tempVector = new Vector<HotSpot>();
						hotSpotDB.put((i-hash.get(temp).elementAt(j)), tempVector);
						hotSpotDB.get((i-hash.get(temp).elementAt(j))).add(tempHotSpot);
					}
				}
			}
		}
		return hotSpotDB;
	}



	/**
	 * combining multiple hot spots into a diagonal according to distance parameters.
	 * @param hotSpotDB all the hotspots
	 * @param T big string (chromosome)
	 * @param P small string (EST)
	 * @param length minimal length of diagonal
	 */
	public static void combineHotSpot (HashMap<Integer, Vector<HotSpot>> hotSpotDB, String T, String P ,int length)
	{
		System.out.println("function: combineHotSpot in DIagonl");
		for(int i=length-T.length(); i<=(P.length()-length); i++)
		{
			if(!(hotSpotDB.get(i).isEmpty()) && hotSpotDB.get(i)!=null)
			{
				for(int j=0; j<(hotSpotDB.get(i).size()-1); j++)
				{
					if(hotSpotDB.get(i).elementAt(j+1)!=null)
					{
						boolean fix = setHotSpot(hotSpotDB.get(i).elementAt(j), hotSpotDB.get(i).elementAt(j+1));
						if(fix==true)
						{
							hotSpotDB.get(i).remove(j+1);
							i--;
						}
					}
				}
			}
		}
	}


	/**
	 * combining hotspots that are on the same diagonal
	 * @param hotSpotDB all the hotspots
	 * @param T big string (chromosome)
	 * @param P small string (EST)
	 * @param length minimal length of diagonal
	 * @return a database of the hotspots sorted by score(length)
	 */
	public static Vector<HotSpot> returMaxHotSpot(HashMap<Integer, Vector<HotSpot>> hotSpotDB,  String T,String P, int length)
	{
		System.out.println("function: returMaxHotSpot in DIagonl");
		Vector<HotSpot> tempSort = new Vector<HotSpot>();
		for(int i=length-T.length(); i<=(P.length()-length); i++)
		{
			if(hotSpotDB.get(i)!=null)
			{
				for(int j=0; j<hotSpotDB.get(i).size(); j++)
				{
					tempSort.add(hotSpotDB.get(i).get(j));
				}
			}
		}

		//########################################
		//		for(int i = 0; i<tempSort.size(); i++)
		//		{
		//			System.out.println(tempSort.elementAt(i).scoreHotSpot);
		//		}
		//########################################
		if(tempSort.size()>1)
		{
			Collections.sort(tempSort);
			hotSpotDB = new HashMap<Integer, Vector<HotSpot>>();
			for(int i=length-T.length(); i<=(P.length()-length); i++)
			{
				Vector<HotSpot> tempVector = new Vector<HotSpot>();
				hotSpotDB.put(i, tempVector);
			}
			//#########################################
			//		System.out.println("tempSort.size() = " + tempSort.size());
			//#########################################
			for (int i = tempSort.size()-1; i>=tempSort.size()-NUM_OF_BEST_SCORING_REGION; i--)
			{
				int diagonal = tempSort.elementAt(i).Index_I_begin-tempSort.elementAt(i).Index_J_begin;
				if(hotSpotDB.get(diagonal)!=null)
				{
					hotSpotDB.get(diagonal).add(tempSort.elementAt(i));
				}
				//			System.out.println("hotSpotDB.get("+diagonal+") in index:  is: " + hotSpotDB.get(diagonal).size());

			}
			for(int i = 0; i<tempSort.size()-length; i++)
			{
				tempSort.remove(i);
			}
			return tempSort;
		}
		else
		{
			return tempSort;
		}
	}

/**
 * considering the best combination of diagonals after having picked the best 10 diagonals
 */
	public  void  findBestCombination ()
	{
		System.out.println("function: findBestCombination in DIagonl");
		Vector<Diagonal> allDiagonal= new Vector<Diagonal>();
		//		System.out.println("this.diagonl.size(): " +this.diagonl.size());
		for (int i = 0; i<this.diagonl.size(); i++)
		{
			Diagonal temp = new Diagonal();
			temp.diagonl.add(this.diagonl.elementAt(i));
			int distanse = Math.abs((this.diagonl.elementAt(i).Index_I_begin-this.diagonl.elementAt(i).Index_J_begin));
			temp.scoreDiagonal = this.diagonl.elementAt(i).scoreHotSpot;
			temp.index_I_begin = this.diagonl.elementAt(i).Index_I_begin;
			temp.index_J_begin = this.diagonl.elementAt(i).Index_J_begin;
			temp.index_I_end = this.diagonl.elementAt(i).Index_I_End;
			temp.index_J_end = this.diagonl.elementAt(i).Index_J_End;
			for(int j =0; j<this.diagonl.size(); j++)
			{
				boolean insert = true;
				//				System.out.println("insex i: "+i+"index j: "+j);
				if(j!=i)
				{
					//					System.out.println("secend: insex i: "+i+ " index j: "+j);
					insert = overLapVector(this.diagonl.elementAt(j), temp.diagonl, distanse, RADIUS);
					if(insert == true)
					{
						temp.diagonl.add(this.diagonl.elementAt(j));
						fixIndex(temp, this.diagonl.elementAt(j));
						temp.fixScore(temp, this.diagonl.elementAt(j), distanse);
					}
				}
			}
			allDiagonal.add(temp);
			//			System.out.println("scoreDiagonal index: "+i+" "+temp.scoreDiagonal);
		}
		//		System.out.println("allDiagonal size = " + allDiagonal.size());
		if(allDiagonal.size()!=0)
		{
			Collections.sort(allDiagonal);
			//			System.out.println("scoreDiagonal index:  "+allDiagonal.lastElement().scoreDiagonal);
			this.diagonl = allDiagonal.lastElement().diagonl;
			this.index_I_begin = allDiagonal.lastElement().index_I_begin;
//			System.out.println("this.index_I_begin:  "+this.index_I_begin);
			this.index_J_begin = allDiagonal.lastElement().index_J_begin;
//			System.out.println("this.index_J_begin:  "+this.index_J_begin);
			this.index_I_end = allDiagonal.lastElement().index_I_end;
//			System.out.println("this.index_I_end:  "+this.index_I_end);
			this.index_J_end = allDiagonal.lastElement().index_J_end;
//			System.out.println("this.index_J_end:  "+this.index_J_end);
			this.scoreDiagonal = allDiagonal.lastElement().scoreDiagonal;
		}

	}

/**
 * find our whether or not two diagonals can exist together
 * @param toInsert candidate diagonal to insert
 * @param vec a collection of hotspots that don't overlap
 * @param distanse allowed distance between hotspots on the same diagonal
 * @param RADIUS distance allowed between two diagonals (horizontal and vertical)
 * @return True if they can exist together, False otherwise
 */
	public static boolean overLapVector(HotSpot toInsert, Vector<HotSpot> vec, int distanse, int RADIUS)
	{
//		System.out.println("function: overLapVector in DIagonl");
		boolean ans = true;
		for(int i=0; i<vec.size(); i++)
		{
			ans = toInsert.overLap(vec.elementAt(i), distanse, RADIUS);
			if(ans==false)
			{
				return ans;
			}
		}
		return ans;
	}


	/**
	 * fixing the coordinates of diagonals after combining them
	 * @param fix the diagonal needed fixing
	 * @param element the newly added hotspot
	 */
	public static void fixIndex(Diagonal fix, HotSpot element)
	{
		//		System.out.println("function: fixIndex in DIagonl");
		fix.index_I_begin = Math.min(fix.index_I_begin, element.Index_I_begin);
		fix.index_I_end = Math.max(fix.index_I_end, element.Index_I_End);
		fix.index_J_begin = Math.min(fix.index_J_begin, element.Index_J_begin);
		fix.index_J_end = Math.max(fix.index_J_end, element.Index_J_End);
	}


	/**
	 * checks whether or not two hotspots can exist together
	 * @param first
	 * @param last
	 * @return
	 */
	public static boolean setHotSpot(HotSpot first, HotSpot last)
	{
		//		System.out.println("function: setHotSpot in DIagonl");
		boolean ans = false;
		if((last.Index_I_begin-first.Index_I_End)<DISTANCE_HOT_SPOT)
		{
			ans = true;
			if(last.Index_I_End>first.Index_I_End)
			{
				first.scoreHotSpot +=last.scoreHotSpot-(last.Index_I_begin-first.Index_I_End+1);
			}
			else
			{
				first.scoreHotSpot +=last.scoreHotSpot-(first.Index_I_End-last.Index_I_begin+1);
			}
			first.Index_I_End = last.Index_I_End;
			first.Index_J_End = last.Index_J_End;
		}

		return ans;
	}



	/**
	 * compares by score
	 */
	public int compareTo(Diagonal o) {
		return this.scoreDiagonal-o.scoreDiagonal;
	}

/**
 * fixing score of a diagonal after combining it with a hotspot
 * @param temp current diagonal
 * @param elementAt newly added hotspot
 * @param distanse distance between the two, needed for penalty in score
 */
	public void fixScore(Diagonal temp, HotSpot elementAt, int distanse) {
		//		System.out.println("function: fixScore in DIagonl");
		this.scoreDiagonal += (elementAt.scoreHotSpot-(Math.abs(elementAt.Index_I_begin-elementAt.Index_J_begin)-distanse));
	}




}
