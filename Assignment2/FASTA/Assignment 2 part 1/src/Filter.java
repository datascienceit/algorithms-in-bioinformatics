import java.io.BufferedWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

/**
 * the actualy filter being used
 * a partial implementation on the fasta algorithm.
 * checks whther or not an EST has an exact match of size LENGTH.
 * if so, performs an alignment on a 10000 bp window around the area
 * @author noam and motz
 *
 */
public class Filter {

	static final int LENGTH = 150;
	static final int  LOWERBOUNDS = 1;
	HashMap<String, Vector<Integer>> sextet_T;
	int countDiagonal;
	int space;

/**
 * constructor
 * @param T
 * @param P
 */
	public Filter(String T, String P)
	{
		this.sextet_T = new HashMap<String, Vector<Integer>>();
		this.setSextet(T,LENGTH);
	}

/**
 * initializes the hash table for current window.
 * @param T
 * @param LENGTH
 */
	public void setSextet(String T, int LENGTH)
	{
		//		System.out.println("function: setSextet in Fasta");
		for(int i=0; i<=T.length()-LENGTH; i++)
		{
			String temp = T.substring(i, i+LENGTH);
			if(this.sextet_T.get(temp)==null)
			{
				Vector<Integer> tempVector = new Vector<Integer>();
				tempVector.add(i);
				this.sextet_T.put(temp,tempVector);
			}
			else
			{
				this.sextet_T.get(temp).add(i);
			}
		}
	}

/**
 * counts the number of times we found exact matches of size LENGTH.
 * allowed further filtration by number of matches.
 * @param hash a hash map of ktups for the chromosome's current window.
 * @param length
 * @param P
 */
	public void countDiagonal(HashMap<String, Vector<Integer>>  hash, int length, String P)
	{
		int indexMatch=-1;
		int ans =0;
		//		System.out.println("function: countDiagonal in DIagonl");
		for(int i=0; i<=P.length()-length; i++)
		{
			String temp = P.substring(i, i+length);
			if(hash.get(temp)!=null)
			{
				ans = ans + hash.get(temp).size();
				for(int j=0; j<hash.get(temp).size(); j++)
				{
					//					System.out.println("find match: "+temp +" in index: "+ hash.get(temp).elementAt(j)+" this temp found: "+ hash.get(temp).size());
					if(hash.get(temp).elementAt(j)>indexMatch)
					{
						indexMatch = hash.get(temp).elementAt(j);
					}
				}
			}
		}
		this.countDiagonal= ans;
		this.space = indexMatch;
	}

	/**
	 * a precise alignment on the area of the match for those ESTs that passed the filtration
	 * @param chromosome
	 * @param ests
	 * @param matrix
	 * @param bw
	 */
	public static void AlignmentAfterFilter(Vector<PartialChromosome> chromosome, Vector<String> ests, ScoringMatrix matrix,BufferedWriter bw)
	{
		//		System.out.println("function: AlignmentAfterFilter");

		GapCell[][] tableGap;
		int counter=0;

		for(int i=0; i<ests.size(); i++) /// TODO fix........................................
		{
			int indexMax = -1;
			int scoretemp = -1;
			for(int j=0; j<chromosome.size(); j++)
			{
				Filter temp = new Filter(chromosome.elementAt(j).Partial, ests.elementAt(i));
				temp.countDiagonal(temp.sextet_T, LENGTH, ests.elementAt(i));
				chromosome.elementAt(j).PartialScore = temp.countDiagonal;
				chromosome.elementAt(j).indexAlignment = temp.space;
				if(chromosome.elementAt(j).PartialScore>scoretemp)
				{
					scoretemp = chromosome.elementAt(j).PartialScore;
					indexMax = j;
				}
			}
			if(indexMax>=0 && chromosome.elementAt(indexMax).PartialScore >= LOWERBOUNDS)
			{
				PartialChromosome sendAlignment = new PartialChromosome(chromosome, indexMax );
				tableGap = new GapCell[sendAlignment.Partial.length()+1][ests.elementAt(i).length()+1];
				Alignments.Gap_PinT_Alignment(sendAlignment, ests.elementAt(i),(i+2), matrix, tableGap, bw);
			}
			else
			{
			//	System.out.println("est number " +(i+2)+" against all partial chromosome no match!!!");
				counter++;
			}
		}
		double temp = ((ests.size()-counter)/ests.size())*100;
		System.out.println("total: "+ ests.size()+" filter: "+(ests.size()-counter)+" filterOut: "+counter+ " "+temp+" %");
	}
}
