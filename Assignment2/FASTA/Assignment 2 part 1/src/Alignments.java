import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.security.AllPermission;
import java.util.Vector;

/**
 * A collection of static alignment functions for PART 1
 * @author noam and motz
 *
 */
public class Alignments {

	static final double A = 0.5;
	static final double B = 0.5;

/**
 * a global alignment implementation.
 * @param s1 first string
 * @param s2 second string
 * @param matrix scoring matrix
 * @param table the dynamic programming table for the algorithm.
 */
	public static void globalAlignment (String s1, String s2, ScoringMatrix matrix, AlignmentCell table[][])
	{
		table[0][0]=new AlignmentCell();
		for (int i=1; i<= s1.length();i++)
		{
			table[i][0]=new AlignmentCell((i-1),0,(table[i-1][0].score+matrix.score(s1.charAt(i-1)+"*")));
		}
		for (int j=1; j<= s2.length();j++)
		{
			table[0][j]=new AlignmentCell(0, (j-1), table[0][j-1].score+matrix.score("*"+s2.charAt(j-1)));
		}
		for (int i=1;i<=s1.length();i++)
		{
			for (int j=1; j<=s2.length();j++)
			{
				if((table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) >= (table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*")) &&
						(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) >=(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))))
				{
					table[i][j] = new AlignmentCell((i-1),(j-1),(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))));
				}
				// insert case
				else if((table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>=(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) &&
						(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>=(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))))
				{
					//					System.out.println("left case");
					table[i][j] = new AlignmentCell((i-1), (j), table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"));
				}
				// deleate case
				else
				{
					//					System.out.println("up case");
					table[i][j] = new AlignmentCell((i), (j-1), table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j)));
				}
			}
		}
		printGlobalAlignment(s1,s2, table);
	}

/**
 * a methoed that prints the alignment from the global alignment algorithm using a full table
 * @param s1 first string
 * @param s2 second string
 * @param table a full table after the dynamic programming
 */
	public static void  printGlobalAlignment(String s1, String s2, AlignmentCell[][] table)
	{
		String T="";
		String P="";
		AlignmentCell run = table[s1.length()][s2.length()];
		int index_P = run.parent_j;
		int index_T = run.parent_I;
		int index_I = s1.length();
		int index_J = s2.length();
		boolean stop = true;
		while (stop)
		{
			//			System.out.println("index_P: "+index_P);
			//			System.out.println("index_T: "+index_T);
			int sum_I = index_I-run.parent_I;
			int sum_J = index_J-run.parent_j;
			if(sum_I==1 && sum_J==1)
			{
				//				System.out.println("diagonal case: "+sum_I+" "+sum_J);
				T = s1.charAt(index_I-1)+T;
				P = s2.charAt(index_J-1)+P;
				run = table[index_T][index_P];
				index_I--;
				index_J--;
			}
			else if(sum_I==0 && sum_J==1)
			{
				//				System.out.println("insert case: "+sum_I+" "+sum_J);
				T = "_"+T;
				P = s2.charAt(index_J-1)+P;
				run = table[index_T][index_P];
				index_J--;
			}
			else if(sum_I==1 && sum_J==0)
			{
				//				System.out.println("deleate case: "+sum_I+" "+sum_J);
				T = s1.charAt(index_I-1)+T;
				P = "_"+P;
				run = table[index_T][index_P];
				index_I--;
			}
			index_T = run.parent_I;
			index_P = run.parent_j;
			if(index_I==0 && index_J==0)
			{
				stop=false;
			}
		}
		System.out.println("> GlobalAlignment:");
		System.out.print("   CHR: ");
		System.out.println(P);
		System.out.print("   EST: ");
		System.out.println(T);
		System.out.println("   SCORE: "+table[s1.length()][s2.length()].score);
	}



/**
 * a local alignment implementation.
 * @param s1 first string
 * @param s2 second string
 * @param matrix scoring matrix
 * @param table the dynamic programming table for the algorithm.
 */
	public static void localAlignment (String s1, String s2, ScoringMatrix matrix, AlignmentCell table[][])
	{
		table[0][0]=new AlignmentCell();
		int max_I=0;
		int max_j=0;
		int max=0;
		for (int i=1; i<= s1.length();i++)
		{
			table[i][0]=new AlignmentCell(i-1,0,0);
		}
		for (int j=1; j<= s2.length();j++)
		{
			table[0][j]=new AlignmentCell(0,j-1,0);
		}
		for (int i=1;i<=s1.length();i++)
		{
			for (int j=1; j<=s2.length();j++)
			{


				if((table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) >= (table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*")) &&
						(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) >=(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))) &&
						table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))>0)
				{
					table[i][j] = new AlignmentCell(i-1, j-1,(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))));
					if(table[i][j].score>=max)
					{
						max=table[i][j].score;
						max_I=i;
						max_j=j;
					}
				}
				// insert
				else if((table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>=(table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) &&
						(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>=(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))) &&
						(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*"))>0)
				{
					table[i][j] = new AlignmentCell(i-1, j,(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*")));
					if(table[i][j].score>=max)
					{
						max=table[i][j].score;
						max_I=i;
						max_j=j;
					}
				}
				// deleate case
				else if((table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))) >= (table[i-1][j-1].score+matrix.score(s1.substring(i-1,i)+s2.substring(j-1,j))) &&
						(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))) >=(table[i-1][j].score+matrix.score(s1.substring(i-1,i)+"*")) &&
						(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j)))>=0)
				{
					table[i][j] = new AlignmentCell(i, j-1,(table[i][j-1].score+matrix.score("*"+s2.substring(j-1,j))));
					if(table[i][j].score>=max)
					{
						max=table[i][j].score;
						max_I=i;
						max_j=j;
					}
				}
				else
				{
					table[i][j] = new AlignmentCell(0, 0, 0);
				}
			}
		}
		printLocalAlignment(s1,s2, table,max_I,max_j);
	}

	/**
	 * a method that restores and prints the alignment from the algorithm
	 * @param s1 first string
	 * @param s2 second string
	 * @param table a full table
	 * @param max_I i value of the cell with the highest score on the table
	 * @param max_J j value of the cell with the highest score on the table
	 */
	public static void printLocalAlignment(String s1, String s2, AlignmentCell[][] table,  int max_I, int max_J)
	{
		String T="";
		String P="";
		AlignmentCell run = table[max_I][max_J];
		int index_P = run.parent_j;
		int index_T = run.parent_I;
		int index_I = max_I;
		int index_J = max_J;
		boolean stop = true;
		while (stop)
		{
			int sum_I = index_I-run.parent_I;
			int sum_J = index_J-run.parent_j;
			if(sum_I==1 && sum_J==1)
			{
				T = s1.charAt(index_I-1)+T;
				P = s2.charAt(index_J-1)+P;
				run = table[index_T][index_P];
				index_I--;
				index_J--;
			}

			else if(sum_I==1 && sum_J==0)
			{
				T = s1.charAt(index_I-1)+T;
				P = "_"+P;
				run = table[index_T][index_P];
				index_I--;
			}

			else if(sum_I==0 && sum_J==1)
			{
				T = "_"+T;
				P = s2.charAt(index_J-1)+P;
				run = table[index_T][index_P];
				index_J--;
			}
			index_T = run.parent_I;
			index_P = run.parent_j;
			if(index_I==0 || index_J==0)
			{
				stop=false;
			}
		}
		System.out.println("> LocalAlignment:");
		System.out.print("   CHR: ");
		System.out.println(P);
		System.out.print("   EST: ");
		System.out.println(T);
		System.out.println("   SCORE: "+table[max_I][max_J].score);
	}



/**
 * the variant we chose best suits the problem. a P in T alignment that allows gaps only on T.
 * @param s1 the part of the chromosome which we align the EST against. according to the current window.
 * @param s2 the EST
 * @param indexEst number of EST from the list
 * @param matrix the scoring matrix
 * @param tableGap the table for the dynamic programming algorithm.
 * @param bw the writer for the html output.
 */
	public static void Gap_PinT_Alignment(PartialChromosome s1, String s2,int indexEst, ScoringMatrix matrix, GapCell[][] tableGap,BufferedWriter bw)
	{
		tableGap[0][0]=new GapCell(0,0,0,0,0);
		for (int i=1; i<= s1.Partial.length();i++)
		{
			tableGap[i][0]=new GapCell(0,0,0,i-1,0);
		}
		for (int j=1; j<= s2.length();j++)
		{
			tableGap[0][j] = new GapCell(tableGap[0][j-1].G+matrix.score("*"+s2.charAt(j-1)),tableGap[0][j-1].E+matrix.score("*"+s2.charAt(j-1)),tableGap[0][j-1].F+matrix.score("*"+s2.charAt(j-1))-A,0,j-1);
		}
		for (int i=1;i<=s1.Partial.length();i++)
		{
			for (int j=1; j<=s2.length();j++)
			{
				tableGap[i][j] = new GapCell(0, 0, 0, 0, 0);
				tableGap[i][j].SetGapCell(
						tableGap[i-1][j-1].V+matrix.score(s1.Partial.substring(i-1,i)+s2.substring(j-1,j)),
						tableGap[i][j-1].V+matrix.score("*"+s2.substring(j-1,j)),
						Math.max(Math.max(tableGap[i-1][j].G, tableGap[i-1][j].E)+w(1), tableGap[i-1][j].F-B),
						tableGap, A, B, i, j);
			}
		}
		double maxScore = tableGap[0][s2.length()].V;
		for(int i = 1; i<= s1.Partial.length();  i++)
		{
			if(maxScore < tableGap[i][s2.length()].V)
			{
				maxScore=tableGap[i][s2.length()].V;
			}
		}
		for(int i =0; i<=s1.Partial.length(); i++)
		{
			if(maxScore==tableGap[i][s2.length()].V)
			{
				printGap_PinT_Alignment(s1,s2, tableGap,i,indexEst , bw);
			}
		}
	}


/**
 * performs the gap alignment only for scoring purposes.
 * @param s1 the part of the chromosome which we align the EST against. according to the current window.
 * @param s2 the EST
 * @param indexEst number of EST from the list
 * @param matrix the scoring matrix
 * @param tableGap the table for the dynamic programming algorithm.
 * @param bw the writer for the html output.
 * @return the score for the current alignment
 */
	public static double Gap_PinT_Alignment1(PartialChromosome s1, String s2,int indexEst, ScoringMatrix matrix, GapCell[][] tableGap,BufferedWriter bw)
	{
		tableGap[0][0]=new GapCell(0,0,0,0,0);
		for (int i=1; i<= s1.Partial.length();i++)
		{
			tableGap[i][0]=new GapCell(0,0,0,i-1,0);
		}
		for (int j=1; j<= s2.length();j++)
		{
			tableGap[0][j] = new GapCell(-j,-j,-j,0,j-1);
		}
		for (int i=1;i<=s1.Partial.length();i++)
		{
			for (int j=1; j<=s2.length();j++)
			{
				tableGap[i][j] = new GapCell(0, 0, 0, 0, 0);
				tableGap[i][j].SetGapCell(
						tableGap[i-1][j-1].V+matrix.score(s1.Partial.substring(i-1,i)+s2.substring(j-1,j)),
						tableGap[i][j-1].V+matrix.score("*"+s2.substring(j-1,j)),
						Math.max(Math.max(tableGap[i-1][j].G, tableGap[i-1][j].E)+w(1), tableGap[i-1][j].F-B),
						tableGap, A, B, i, j);
			}
		}
		double maxScore = tableGap[0][s2.length()].V;
		for(int i = 1; i<= s1.Partial.length();  i++)
		{
			if(maxScore < tableGap[i][s2.length()].V)
			{
				maxScore=tableGap[i][s2.length()].V;
			}
		}
		return maxScore;
	}



/**
 * a private method that restores the alignment from the matrix calculated already
 * @param s1 the part of the chromosome which we align the EST against. according to the current window.
 * @param s2 the EST
 * @param tableGap the table for the dynamic programming algorithm.
 * @param index_i the i value of the cell with the highest score from the (according to P in T algorithm)
 * @param indexEst number of EST from the list
 * @param bw the writer for the html output.
 */
	private static void printGap_PinT_Alignment(PartialChromosome s1, String s2, GapCell[][] tableGap,  int index_i,  int indexEst,BufferedWriter bw)
	{
		Vector<Integer> index = new Vector<Integer>();
		int begin = -1;
		String T="";
		String P="";
		GapCell run = tableGap[index_i][s2.length()];
		int index_T = run.parent_I;
		int index_P = run.parent_j;
		int index_I = index_i;
		int index_J = s2.length();
		//		System.out.println("index_P: "+index_P);
		//		System.out.println("index_T: "+index_T);
		//		System.out.println("sum_I: " + (index_I-run.parent_I));
		//		System.out.println("sum_J: "+(index_J-run.parent_j));
		boolean stop = true;
		while (stop)
		{
			//			System.out.println("index_I: "+index_I);
			//			System.out.println("index_J: "+index_J);
			//			System.out.println("index_T: "+index_T);
			//			System.out.println("index_P: "+index_P);
			//			System.out.println("run.parent_I: "+run.parent_I);
			//			System.out.println("run.parent_j: "+run.parent_j);

			int sum_I = index_I-index_T;
			int sum_J = index_J-index_P;

			//			System.out.println("sum_I: "+sum_I);
			//			System.out.println("sum_J: "+sum_J);

			if(sum_I==1 && sum_J==1)
			{
				//				System.out.println("diagonal case: "+sum_I+" "+sum_J);
				if(s1.Partial.charAt(index_I-1)=='A' && s2.charAt(index_J-1)=='G')
				{
					T ="<font size=\"6\" color=\"red\">A</font>"+T;
					P ="<font size=\"6\" color=\"red\">G</font>"+P;
					run = tableGap[index_T][index_P];
					index_I--;
					index_J--;
					index.add(s1.indexBegin+index_I);
				}
				else
				{
					T = s1.Partial.charAt(index_I-1)+T;
					P = s2.charAt(index_J-1)+P;
					run = tableGap[index_T][index_P];
					index_I--;
					index_J--;
				}
			}
			else if(sum_I==0 && sum_J==1)
			{
				//				System.out.println("insert case: "+sum_I+" "+sum_J);
				T = "_"+T;
				P = s2.charAt(index_J-1)+P;
				run = tableGap[index_T][index_P];
				index_J--;
			}
			else if(sum_I>=1 && sum_J==0)
			{
				//				System.out.println("deleate case: "+sum_I+" "+sum_J);
				for(int k=0; k<sum_I; k++)
				{
					T = s1.Partial.charAt(index_I-1)+T;
					P = "_"+P;
					index_I--;
				}
				run = tableGap[index_T][index_P];
			}
			else if(sum_I>=1 && sum_J==1)
			{
				for(int k=1; k<sum_I; k++)
				{
					T = s1.Partial.charAt(index_I-1)+T;
					P = "_"+P;
					index_I--;
				}

			}
			index_T = run.parent_I;
			index_P = run.parent_j;
			if(index_J==0)
			{
				begin=index_I;
				stop=false;
			}
			//			if(index_I==0 && index_J==0)
			//			{
			//				stop=false;
			//			}
		}
		try {
			bw.write("<dt>> EST_"+indexEst+"</dt>");
			bw.write("<dd>CHR:"+T+ "</dd>");
			bw.write("<dd>EST:"+P+ "</dd>");
			bw.write("<br/>");
			bw.write("<dl/>");
			bw.write("<dt>Score:"+tableGap[index_i][s2.length()].V+"<dt>");
			bw.write("<dt>Chromosome indices:"+(s1.indexBegin+begin)+"-"+(s1.indexBegin+index_i)+"</dt>");
			bw.write("<dt>Sites in chromosome:</dt>");
			bw.write("<dd><ol>");
			for(int k=0; k<index.size(); k++)
			{
				bw.write("<li>"+index.elementAt(k)+"</li>");
			}
			bw.write("</ol></dd>");
			bw.write("</dl>");
			bw.write("<br/>");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("> EST_"+indexEst);
//		System.out.println("   CHR: ");
//		System.out.println("   "+T);
//		System.out.println("   EST: ");
//		System.out.println("   "+P);
//		System.out.println();
//		System.out.println();
//		System.out.println("Score: "+tableGap[index_i][s2.length()].V);
//		System.out.println("Chromosome indices: "+(s1.indexBegin+begin)+" - "+(s1.indexBegin+index_i));
//		System.out.println("Sites in chromosome:");
//		System.out.println();

	}


/**
 * the affine gap scoring function
 * @param i size of gap
 * @return gap penalty for a gap of size i
 */
	private static double w (int i)
	{
		return -A-B*i;
	}

}
